<div class='mask-modal mask-modal-inclui-operacao'>
  <div class='conteiner-modal'>
    <span class='modal-title'>INSERIR OPERAÇÃO<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
    <div class='content-modal'>
      <form id="form-inclui-operacao" class="aba-formulario">
        <input type="hidden" class="valida-input-nome-nova-operacao" value="" />
        <input type="hidden" class="valida-input-tipo-nova-operacao" value="" />
        <div class="row">
          <div class="half-col-inp left">
            <label>Nome da nova operação</label>
            <input type="text" class="inp input-nome-nova-operacao" name="nome-nova-operacao" placeholder="Nome da nova operação" />
          </div>
          <div class="half-col-inp right">
            <label>Tipo da operação/área</label>
            <div class="container-simple-select select-tipo-operacao">
              <span class="trigger-simple-select">Selecione...<i class="fa fa-caret-down" aria-hidden="true"></i></span>
              <div class="row">
                <ul id="simple-select" class="simple-select lista-tipo-operacoes">
                  <li id="1" rel="callcenter">Call Center</li>
                  <li id="2" rel="servicos">Serviços</li>
                  <li id="3" rel="operacional">Operacional</li>
                  <li id="4" rel="cliente">Cliente</li>
                  <li id="5" rel="diretoria">Diretoria/Gerência</li>
                </ul>
              </div>
              <input type="hidden" name="tipo-nova-operacao" class="tipo-nova-operacao"></div>
          </div>
        </div>
        <div class="row mt20">
          <button type="button" class="btn btn-edit left btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>FECHAR</button>
          <div class='ct-mask-btn-inclui-operacao right'>
            <div class="mask-block"></div>
            <button type="button" class="btn btn-add left mr20" id="btn-inclui-operacao"><i class="fa fa-plus" aria-hidden="true"></i>INCLUIR NOVA OPERAÇÃO</button>
          </div>
        </div>
      </form>

      <div class="aba-formulario result-inclui-operacao" id="result-inclui-operacao">
        <div class='mensagem-result-inclui-operacao mensagem-result-inclui-operacao-enviando'>
          <div class='left mr30 ct-msg-icon-inclui-post'>
            <i class="fa fa-users icon-msg-post icon-publicando" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-post'>
            <span class='msg-inclui-post'>Incluindo operação... Por favor, aguarde.</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-operacao mensagem-result-inclui-operacao-sucesso'>
          <div class='left mr30 ct-msg-icon-inclui-post'>
            <i class="fa fa-check icon-msg-post icon-publicado" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-post'>
            <span class='msg-inclui-post'>Pronto! Nova operação cadastrada com sucesso!</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-operacao mensagem-result-inclui-operacao-erro'>
          <div class='left mr30 ct-msg-icon-inclui-post'>
            <i class="fa fa-frown-o icon-msg-post icon-erro" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-post'>
            <span class='msg-inclui-post'>Deu um erro ao cadastrar. Caso continue, <a title='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' alt='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' href='mailto:sssantos@stefanini.com'>clique aqui</a>.</span>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
