<div class='mask-modal mask-modal-inclui-gestor'>
  <div class='conteiner-modal'>
    <span class='modal-title'>INCLUIR GESTOR<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
    <div class='content-modal'>
      <form id="form-inclui-gestor" class="aba-formulario">
        <input type="hidden" class="valida-input-nome-novo-gestor" value="" />
        <input type="hidden" class="valida-select-gestor-operacao" value="" />
        <div class="row">
          <div class="half-col-inp left">
            <div class="row">
              <label>Nome do novo gestor</label>
              <input type="text" class="inp input-nome-novo-gestor" name="nome-novo-gestor" placeholder="Nome do novo gestor" />
            </div>
            <div class="row mt20">
              <label>E-mail do novo gestor</label>
              <input type="text" class="inp input-email-novo-gestor" name="email-novo-gestor" placeholder="Nome do novo gestor" />
            </div>
          </div>
          <div class="half-col-inp right">
            <div class="container-simple-select select-gestor-operacao">
              <div class="row">
                <?php operacoesLista($PDO); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt20">
          <button type="button" class="btn btn-edit left btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>FECHAR</button>
          <div class='ct-mask-btn-inclui-gestor right'>
            <div class="mask-block"></div>
            <button type="button" class="btn btn-add left mr20" id="btn-inclui-gestor"><i class="fa fa-plus" aria-hidden="true"></i>INCLUIR NOVA OPERAÇÃO</button>
          </div>
        </div>
      </form>
      
      <div class="aba-formulario result-inclui-gestor" id="result-inclui-gestor">
        <div class='mensagem-result-inclui-gestor mensagem-result-inclui-gestor-enviando'>
          <div class='left mr30 ct-msg-icon-inclui-gestor'>
            <i class="fa fa-user icon-msg-post icon-publicando" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-gestor'>
            <span class='msg-inclui-gestor'>Incluindo gestor... Por favor, aguarde.</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-gestor mensagem-result-inclui-gestor-sucesso'>
          <div class='left mr30 ct-msg-icon-inclui-gestor'>
            <i class="fa fa-check icon-msg-post icon-publicado" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-gestor'>
            <span class='msg-inclui-gestor'>Pronto! Novo gestor cadastrado com sucesso!</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-gestor mensagem-result-inclui-gestor-erro'>
          <div class='left mr30 ct-msg-icon-inclui-gestor'>
            <i class="fa fa-frown-o icon-msg-post icon-erro" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-gestor'>
            <span class='msg-inclui-gestor'>Deu um erro ao cadastrar. Caso continue, <a title='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' alt='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' href='mailto:sssantos@stefanini.com'>clique aqui</a>.</span>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
