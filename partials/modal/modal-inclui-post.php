<div class='mask-modal mask-modal-inclui-post'>
  <div class='conteiner-modal'>
    <span class='modal-title'>NOVA PUBLICAÇÃO<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
    <div class='content-modal'>
      <form id='form-inclui-post'>
        <!-- hidden inputs -->
        <input type="hidden" class="id-usuario-logado" name="id-usuario-logado" value="<?php echo $_SESSION['id_usuario'];?>" />
        <input type="hidden" class="tipo-usuario-logado" name="tipo-usuario-logado" value="<?php echo $_SESSION['tipo_usuario'];?>" />

        <!-- Passo 1 -->
        <div class="aba-formulario novo-post-passo-1">
          <div class="line full">
            <div class="half-col-inp left">
              <label>Título da publicação</label>
              <input type="text" class="inp input-titulo-post" name="titulo-post" placeholder="Título da publicação" />
            </div>
            <div class="half-col-inp right select-operacoes-inclui-post">
              <?php operacoesLista($PDO); ?>
            </div>
          </div>
          <!-- tags com as operacoes selecionadas -->
          <div class='row operacoes-selecionadas-post'>
            <input type="hidden" name="novo-post-operacoes" class="novo-post-operacoes" />
          </div>
          <div class="line full mt20">
            <div class="left full">
              <label><span class='left'>Título da publicação</span><span class="count-chars-textarea">Restam <span class='chars-count-red'>210</span> caracteres...</span></label>
              <textarea type="text" class="textarea textarea-texto-post" maxlength="210" name="texto-post" placeholder="Texto da publicação"></textarea>
            </div>
          </div>

          <div class="row mt20 ct-btn-novo-post-passo-2">
            <input type="hidden" class="valida-input-titulo-post" value="" />
            <input type="hidden" class="valida-tags-operacoes" value="" />
            <input type="hidden" class="valida-textarea-texto-post" value="" />
            <!--<button type="button" class="btn btn-edit left btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>FECHAR A JANELA</button>-->
            <div class="left">
              <button type="button" class="btn btn-edit left btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>FECHAR</button>
            </div>
            <div class="right">
              <div class='mask-block'></div>
              <button type="button" class="btn btn-green-ok right btn-steps-modal" id="chkTags" rel='novo-post-passo-2'><i class="fa fa-arrow-right" aria-hidden="true"></i>AVANÇAR</button>
            </div>
          </div>
        </div>

        <!-- Passo 2 -->
        <div class="aba-formulario novo-post-passo-2">
          <div class="line full">
            <div class="half-col-inp left">
              <div class="row">
                <label>Imagem de destaque</label>
                <textarea name="post_editor" class="textarea post_editor" id="post_editor"></textarea>
              </div>
              <div class="row mt20 only-admin">
                <label>Status do post</label>
                <div class="left mr20">
                  <div class="left mr10">
                    <span class="label-no-float left mt5 mr5">Ativo</span>
                    <input type='radio' class='post-status' name='post-status' value='1' rel='ativo'  />
                  </div>
                </div>
                <div class="left">
                  <div class="left mr10">
                    <span class="label-no-float left mt5 mr5">Inativo</span>
                    <input type='radio' class='post-status' name='post-status' value='2' rel='inativo' checked="checked" />
                  </div>
                </div>
              </div>
              <div class="row mt20">
                <div class="left mr10">
                  <label>Link externo - Título</label>
                  <input type='text' class='inp' name='titulo-link-externo-post' />
                  <label class="mt20">Link externo - URL(Endereço)</label>
                  <input type='text' class='inp' name='url-link-externo-post' />
                </div>
              </div>
            </div>
            <div class="half-col-inp right">
              <label>Selecione o box da publicação</label>
              <div class="header-box-noticia">
                <span class="bullet-red"></span>
                <span class="bullet-yellow"></span>
                <span class="bullet-green"></span>
              </div>
              <div class="container-selec-box-noticia">
                <div class="box-pos box-pos-1" rel="1">
                  <span class="numero-box">1</span>
                  <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                </div>
                <div class="col-box-pos-right">
                  <div class="box-pos box-pos-2" rel="2">
                    <span class="numero-box">2</span>
                    <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                  </div>
                  <div class="box-pos box-pos-3" rel="3">
                    <span class="numero-box">3</span>
                    <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                  </div>
                  <div class="box-pos box-pos-4" rel="4">
                    <span class="numero-box">4</span>
                    <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                  </div>
                </div>
                <input type="hidden" class="inp-hidden-box-pos" name="inp-box-pos" />
              </div>
            </div>
          </div>

          <div class="row mt20">
            <button type="button" class="btn btn-edit left btn-steps-modal" rel='novo-post-passo-1'><i class="fa fa-arrow-left" aria-hidden="true"></i>VOLTAR</button>

            <div class='right ct-btn-publicar-post'>
              <div class='mask-block'></div>
              <button type="button" class="btn btn-green-ok right btn-steps-modal btn-publicar-post" id="btn-publicar-post"><i class="fa fa-plus" aria-hidden="true"></i>PUBLICAR</button>
            </div>
          </div>
        </div>
      </form>

      <div class="aba-formulario result-inclui-post">
        <div class='mensagem-result-inclui-post mensagem-result-inclui-post-enviando'>
          <div class='left mr30 ct-msg-icon-inclui-post'>
            <i class="fa fa-clipboard icon-msg-post icon-publicando" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-post'>
            <span class='msg-inclui-post'>Publicando... Por favor, aguarde.</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-post mensagem-result-inclui-post-sucesso'>
          <div class='left mr30 ct-msg-icon-inclui-post'>
            <i class="fa fa-check icon-msg-post icon-publicado" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-post'>
            <span class='msg-inclui-post'>Pronto! Sua publicação foi enviada com sucesso!</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-post mensagem-result-inclui-post-erro'>
          <div class='left mr30 ct-msg-icon-inclui-post'>
            <i class="fa fa-frown-o icon-msg-post icon-erro" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-post'>
            <span class='msg-inclui-post'>Deu um erro ao publicar. Caso continue, <a title='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' alt='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' href='mailto:sssantos@stefanini.com'>clique aqui</a>.</span>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
