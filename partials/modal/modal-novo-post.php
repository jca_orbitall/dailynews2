<div class='mask-modal mask-modal-novo-post'>
  <div class='conteiner-modal'>
    <span class='modal-title'>NOVA PUBLICAÇÃO<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
    <div class='content-modal'>
      <form>
        <!-- Passo 1 -->
        <div class="aba-formulario novo-post-passo-1">
          <div class="line full">
            <div class="half-col-inp left">
              <label>Título da publicação</label>
              <input type="text" class="inp" name="titulo-post" placeholder="Título da publicação" />
            </div>
            <div class="half-col-inp right">
              <?php operacoesLista($PDO); ?>
            </div>
          </div>
          <!-- tags com as operacoes selecionadas -->
          <div class='row operacoes-selecionadas-post'></div>
          
          <div class="line full mt20">
            <div class="left full">
              <label><span class='left'>Título da publicação</span><span class="count-chars-textarea">Restam <span class='chars-count-red'>XX</span> caracteres...</span></label>
              <textarea type="text" class="textarea textarea-texto-post" name="texto-post" placeholder="Texto da publicação"></textarea>
            </div>
          </div>

          <div class="row mt20">
            <!--<button type="button" class="btn btn-edit left btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>FECHAR A JANELA</button>-->
            <button type="button" class="btn btn-green-ok right btn-steps-modal" rel='novo-post-passo-2'><i class="fa fa-arrow-right" aria-hidden="true"></i>AVANÇAR</button>
          </div>
        </div>

        <!-- Passo 2 -->
        <div class="aba-formulario novo-post-passo-2">
          <div class="line full">
            <div class="half-col-inp left">
              <label>Imagem de destaque</label>
              <textarea name="post_editor" class="textarea post_editor" id="post_editor"></textarea>
            </div>
            <div class="half-col-inp right">
              <label>Selecione o box da publicação</label>
              <div class="header-box-noticia">
                <span class="bullet-red"></span>
                <span class="bullet-yellow"></span>
                <span class="bullet-green"></span>
              </div>
              <div class="container-selec-box-noticia">
                <div class="box-pos box-pos-1" rel="1">
                  <span class="numero-box">1</span>
                  <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                </div>
                <div class="col-box-pos-right">
                  <div class="box-pos box-pos-2" rel="2">
                    <span class="numero-box">2</span>
                    <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                  </div>
                  <div class="box-pos box-pos-3" rel="3">
                    <span class="numero-box">3</span>
                    <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                  </div>
                  <div class="box-pos box-pos-4" rel="4">
                    <span class="numero-box">4</span>
                    <i class="fa fa-check-circle box-pos-check" aria-hidden="true"></i>
                  </div>
                </div>
                <input type="hidden" class="inp-hidden-box-pos" name="inp-hidden-box-pos" />
              </div>
            </div>
          </div>

          <div class="row mt20">
            <button type="button" class="btn btn-edit left btn-steps-modal" rel='novo-post-passo-1'><i class="fa fa-arrow-left" aria-hidden="true"></i>VOLTAR</button>
            <button type="button" class="btn btn-green-ok right btn-steps-modal btn-publicar-post" rel='novo-post-passo-2'><i class="fa fa-plus" aria-hidden="true"></i>PUBLICAR</button>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
