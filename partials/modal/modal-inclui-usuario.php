<div class='mask-modal mask-modal-inclui-usuario'>
  <div class='conteiner-modal'>
    <span class='modal-title'>INSERIR USUÁRIO<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
    <div class='content-modal'>
      <form id="form-inclui-usuario">
        <input type="hidden" class="valida-input-nome-novo-usuario" value="" />
        <input type="hidden" class="valida-input-email-novo-usuario" value="" />
        <div class="line">
          <div class="left">
            <div class="left half-col-inp">
              <label>Nome do novo usuário</label>
              <input type="text" class="inp input-nome-novo-usuario" name="nome-novo-usuario" placeholder="Nome do novo usuário" />
            </div>
            <div class="container-btn-tip" id="tip-usuario">
              <div class="btn-tip">
                <div class="trigger-btn-tip btn-tip-usuario left ml5">
                  <i class="fa fa-question-circle" aria-hidden="true"></i>
                </div>
                <div class="bubble-tip">
                  <div class="bubble-tip-icon">
                    <i class="fa fa-exclamation" aria-hidden="true"></i>
                  </div>
                  <div class="bubble-tip-text">
                    O usuário será indicado a alterar a senha no primeiro acesso.
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="half-col-inp right ml40">
            <label>E-mail do novo usuário</label>
            <input type="text" class="inp input-email-novo-usuario" name="email-novo-usuario" placeholder="E-mail do novo usuário" />
          </div>
        </div>

        <div class="row mt20">
          <div class="left">
            <button type="button" class="btn btn-edit left btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>FECHAR</button>
          </div>
          <div class='ct-mask-btn-inclui-usuario right'>
            <div class="mask-block"></div>
            <button type="button" class="btn btn-add left mr20" id="btn-inclui-usuario" rel="usuario"><i class="fa fa-plus" aria-hidden="true"></i>INCLUIR NOVO USUÁRIO</button>
          </div>
        </div>
      </form>
      <div class="aba-formulario result-inclui-usuario" id="result-inclui-usuario">
        <div class='mensagem-result-inclui-usuario mensagem-result-inclui-usuario-enviando'>
          <div class='left mr30 ct-msg-icon-inclui-usuario'>
            <i class="fa fa-user icon-msg-post icon-publicando" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-usuario'>
            <span class='msg-inclui-usuario'>Incluindo usuário... Por favor, aguarde.</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-usuario mensagem-result-inclui-usuario-sucesso'>
          <div class='left mr30 ct-msg-icon-inclui-usuario'>
            <i class="fa fa-check icon-msg-post icon-publicado" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-usuario'>
            <span class='msg-inclui-usuario'>Pronto! Novo usuário cadastrado com sucesso!</span>
          </div>
        </div>
        <div class='mensagem-result-inclui-usuario mensagem-result-inclui-usuario-erro'>
          <div class='left mr30 ct-msg-icon-inclui-usuario'>
            <i class="fa fa-frown-o icon-msg-post icon-erro" aria-hidden="true"></i>
          </div>
          <div class='left ct-msg-txt-inclui-usuario'>
            <span class='msg-inclui-usuario'>Deu um erro ao cadastrar. Caso continue, <a title='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' alt='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' href='mailto:sssantos@stefanini.com'>clique aqui</a>.</span>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
