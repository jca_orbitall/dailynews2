<div class='mask-modal mask-modal-excluir'>
  <div class='conteiner-modal'>
    <span class='modal-title'>EXCLUIR? TEM CERTEZA?<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
    <div class='content-modal'>
      <div class="line">
        <div class="left">
          <h6 class='excluir-msg-certeza'>Tem certeza que deseja excluir?</h6>
        </div>
      </div>
      <div class="row mt20">
        <button type="button" class="btn btn-error left mr20" id="excluirDefinitivo"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>DESEJO EXCLUIR</button>
        <button type="button" class="btn btn-edit right btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>VOU FECHAR E VOLTAR</button>
      </div>
    </div>
  </div>
</div>
