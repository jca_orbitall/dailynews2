<div class='mask-modal mask-modal-editar-operacao'>
  <div class='conteiner-modal'>
    <span class='modal-title'>EDITAR OPERAÇÃO<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
    <div class='content-modal'>
      <form>
        <div class="line">
          <div class="left mr20">
            <label>Nome da nova operação</label>
            <input type="text" class="inp" name="" placeholder="Nome da nova operação" />
          </div>
        </div>
        <div class="row mt20">
          <button type="button" class="btn btn-add left mr20"><i class="fa fa-save" aria-hidden="true"></i>SALVAR ALTERAÇÕES</button>
          <button type="button" class="btn btn-error left mr20" id="excluirOperacao"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>EXCLUIR GESTOR</button>
          <button type="button" class="btn btn-edit left btn-close-modal-big"><i class="fa fa-arrow-left" aria-hidden="true"></i>FECHAR</button>
        </div>
      </form>
    </div>
  </div>
</div>
