<?php include "partials/modal/modal-inclui-usuario.php";?>

<div class="main-admin-content container-usuarios">
  <h6 class="tit-sec-adm">Usuários</h6>
  <div class="container-exibe-content-admin">
    <form id="form-usuarios">
      <div class='row'>
        <!--<div class="select-1 left operacoes-post trigger-lista-gestores-operacao">
          <?php operacoesLista($PDO); ?>
        </div>-->
        <div class='left'>
          <button type="button" class="btn btn-add left mr20" rel="usuario" id="btn-trigger-inclui-usuario"><i class="fa fa-plus" aria-hidden="true"></i>INCLUIR USUÁRIO</button>
        </div>
      </div>
      <div class="result-gestores">
        <?php listaTodosUsuarios($PDO); ?>
      </div>
    </form>
  </div>
</div>
