<?php include "partials/modal/modal-inclui-gestor.php";?>
<?php include "partials/modal/modal-exclui-gestor.php";?>

<div class="main-admin-content container-gestores">
  <h6 class="tit-sec-adm">Gestores</h6>
  <div class="container-exibe-content-admin">
    <form id="form-operacoes">
      <div class='row'>
        <!--<div class="select-1 left operacoes-post trigger-lista-gestores-operacao">
          <?php operacoesLista($PDO); ?>
        </div>-->
        <div class='left'>
          <button type="button" class="btn btn-add left mr20" rel="gestor" id="btn-trigger-inclui-gestor"><i class="fa fa-plus" aria-hidden="true"></i>INCLUIR GESTOR</button>
        </div>
      </div>
      <div class="result-gestores">
        <?php listaTodosGestores($PDO); ?>
      </div>
    </form>
  </div>
</div>
