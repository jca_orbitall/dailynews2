<?php include "partials/modal/modal-inclui-post.php";?>
<?php include "partials/modal/modal-edita-post.php";?>

<div class="main-admin-content container-posts">
  <h6 class="tit-sec-adm">Posts</h6>
  <div class="container-exibe-content-admin">
    <form id="form-operacoes">
      <div class="select-1 left">
        <?php //listaPosts($PDO); ?>
      </div>
      <div class="row">
        <div class="select-1 left operacoes-post trigger-lista-operacoes-posts">
          <?php operacoesLista($PDO); ?>
        </div>
        <div class='left mt15 ml20'>
          <button type="button" class="btn btn-add left mr20" rel="post" id="btn-trigger-inclui-usuario"><i class="fa fa-plus" aria-hidden="true"></i>NOVA PUBLICAÇÃO</button>
        </div>
      </div>

      <div class="result-posts">
        <?php listaTodosPosts($PDO); ?>
      </div>
    </form>
  </div>
</div>
