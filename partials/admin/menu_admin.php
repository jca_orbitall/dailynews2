<aside class="container-menu-admin">
  <nav>
    <ul>
      <li class="btn-menu-admin" rel="posts">
        <i class="fa fa-newspaper-o" aria-hidden="true"></i>Posts
      </li>
      <li class="btn-menu-admin" rel="operacoes">
        <i class="fa fa-users" aria-hidden="true"></i>Operações
      </li>
      <li class="btn-menu-admin" rel="gestores">
        <i class="fa fa-user-plus" aria-hidden="true"></i>Gestores
      </li>
      <li class="btn-menu-admin" rel="usuarios">
        <i class="fa fa-user" aria-hidden="true"></i>Usuários
      </li>
      <li class="btn-menu-admin" rel="sair">
        <i class="fa fa-sign-out" aria-hidden="true"></i>Sair
      </li>
    </ul>
  </nav>
</aside>
