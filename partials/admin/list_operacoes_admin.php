<?php include "partials/modal/modal-inclui-operacao.php";?>
<?php include "partials/modal/modal-editar-operacao.php";?>

<div class="main-admin-content container-operacoes">
  <h6 class="tit-sec-adm">Operações</h6>
  <div class="container-exibe-content-admin">
    <form id="form-operacoes">
      <div class='row'>
        <div class='left'>
          <button type="button" class="btn btn-add left mr20" rel="operacao" id="btn-trigger-inclui-operacao"><i class="fa fa-plus" aria-hidden="true"></i>INCLUIR OPERAÇAO</button>
        </div>
      </div>
      <div class='result-operacoes'>
        <?php listaOperacoesTabela($PDO); ?>
      </div>
    </form>
  </div>
</div>
