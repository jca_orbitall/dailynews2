<footer></footer>

<!-- JS -->
<?php include "partials/js.php";?>
<script src="src/js/ckeditor/ckeditor.js"></script>
<script>
  $(window).load(function() {
    CKEDITOR.replace( 'post_editor', {
      extraPlugins : 'base64image',
      height:['0px']
    });
  });
</script>
