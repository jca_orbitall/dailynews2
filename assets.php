<?php include "partials/head.php"; ?>

  <body>
    <!-- Loader -->
    <?php include "partials/loader.php"; ?>

    <!-- Site Container -->
    <div class="site">
      <!-- Loader -->
      <?php include "partials/header.php"; ?>

      <!-- Home -->
      <section class="container-content">
        <div class="container">

          <!-- Main content -->
          <div class="main-content">
            <div class="row mt50">
              <button type="button" class="btn btn-ok"><i class="fa fa-check" aria-hidden="true"></i>OK</button>
              <button type="button" class="btn btn-add"><i class="fa fa-plus" aria-hidden="true"></i>INCLUIR OPERAÇAO</button>
              <button type="button" class="btn btn-error" id="excluirOperacao"><i class="fa fa-trash-o" aria-hidden="true"></i>EXCLUIR</button>
              <button type="button" class="btn btn-error" id="excluirOperacao"><i class="fa fa-exclamation-circle" aria-hidden="true"></i>ERROR</button>
              <button type="button" class="btn btn-edit"><i class="fa fa-pencil" aria-hidden="true"></i>EDITAR</button>
              <button type="button" class="btn btn-warning"><i class="fa fa-exclamation" aria-hidden="true"></i>WARNING</button>
              <button type="button" class="btn btn-alert"><i class="fa fa-bell-o" aria-hidden="true"></i>ALERT</button>
            </div>
          </div>
        </div>
      </section>

      <!-- Footer -->
      <?php include "partials/footer.php"; ?>
    </div>
    <!-- Site Container -->

  </body>
</html>
