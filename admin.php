<?php
  include "partials/admin/head_admin.php";
  include "check_login.php";

  if($_SESSION['id_usuario'] == '1'){
    $isMaster = 'master';
  } else {
    $isMaster = '';
  }
?>

  <body class="tipo-usuario-<?php echo $_SESSION['tipo_usuario']; ?>" id='<?php echo $isMaster;?>'>
    <div class='mask-modal mask-modal-sair'>
      <div class='conteiner-modal'>
          <span class='modal-title'>VAI EMBORA? :(<button class="fa fa-close btn-close-modal" aria-hidden="true"></button></span>
          <div class='mask-modal-sair-content'></div>
      </div>
    </div>

    <!-- Loader -->
    <?php include "partials/loader.php"; ?>

    <!-- Site Container -->
    <div class="site">
      <?php include "partials/modal/modal-excluir.php"; ?>

      <!-- Header Admin -->
      <?php include "partials/admin/header_admin.php"; ?>

      <!-- Home -->
      <section class="container-content">
        <div class="container">
          <!-- Header Content / Menu -->
          <?php
            include "partials/admin/header_content_admin.php"; //Header Content
            include "partials/admin/menu_admin.php"; //Menu
          ?>

          <div class="content-admin">
            <!-- Posts -->
            <?php include "partials/admin/list_posts_admin.php"; ?>

            <!-- Operações -->
            <?php include "partials/admin/list_operacoes_admin.php"; ?>

            <!-- Gestores -->
            <?php include "partials/admin/list_gestores_admin.php"; ?>

            <!-- Operações -->
            <?php include "partials/admin/list_usuarios_admin.php"; ?>
          </div>
        </div>
      </section>

      <!-- Footer -->
      <?php include "partials/admin/footer_admin.php"; ?>
    </div>
    <!-- Site Container -->

  </body>
</html>
