<?php
	include "_config/config.php";

	$data = date("d/m/Y");
	$nome = utf8_decode($_POST['inp-name']);
	$email = $_POST['inp-mail'];
	$mensagem = utf8_decode($_POST['txt-msg']);
	$valida_formulario = $_POST['valida-formulario'];

	if ($nome == "") {
		$return = array("b"=>false);
	} else if ($valida_formulario != "") {
		$return = array("b"=>false);
	} else if(!preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/', $email)) {
		echo "<script>window.alert('Insira seu e-mail corretamente!'); history.back(1);</script>";
		$return = array("b"=>false);
	} else if (preg_match("/[\n\r%]/", $nome) ) {
		$return = array("b"=>false);
	} else if (preg_match("/[\n\r%]/", $email) ) {
		$return = array("b"=>false);
	} else if (preg_match("/[%]/", $mensagem) ) {
		$return = array("b"=>false);
	} else if (preg_match("/[\n\r%]/", $data) ) {
		$return = array("b"=>false);
	} else if(strlen($nome)) {
		$return = array("b"=>true);

		//Envio do e-mail com o comentário
		$para = "hello@joaocarlosalves.com";

		$texto = "<strong>Nome:</strong> \r\n".$nome;
		$texto .= "<p><strong>e-mail:</strong> \r\n".$email;
		$texto .= "<p><strong>Data: </strong>\r\n".$data;
		$texto .= "<p><strong>Mensagem: </strong>\r\n".$mensagem;

		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
		$headers .= "Reply-To: ".$email."\r\n";
		$headers .= "From: ".$email."\r\n";

		$msgContato = wordwrap($texto, 80, "\r\n");

		mail($para, "Contato de: ".$nome." - JCA", $msgContato, $headers);
	} else {
		$return = array("b"=>false);
		die("Virus do inferno");
	}
?>
