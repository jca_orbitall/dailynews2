<?php
  session_start();

  //Banco de dados
  include "inc/_db_conn.php";

  //Verificando se os campos vindo do POST existem.
  if(isset($_POST['acao'])) {
    $acao = $_POST['acao'];
  }
  if(isset($_POST['id_operacao'])) {
     $operacao = $_POST['id_operacao'];
  }
  if(isset($_POST['id_gestor'])) {
    $gestor = $_POST['id_gestor'];
  }

  //Novo post
  if (isset($_POST['titulo-post'])) {
    $titulo_post = $_POST['titulo-post'];
  }
  if (isset($_POST['post_editor'])) {
    $imagem_post = $_POST['post_editor'];
  }
  if (isset($_POST['texto-post'])) {
    $texto_post = $_POST['texto-post'];
  }
  if (isset($_POST['novo-post-operacoes'])) {
    $novo_post_operacoes = $_POST['novo-post-operacoes'];
  }
  if (isset($_POST['inp-box-pos'])) {
    $inp_box_pos = $_POST['inp-box-pos'];
  }
  if (isset($_POST['post-status'])) {
    $post_status = $_POST['post-status'];
  }
  if (isset($_POST['id-usuario-logado'])) {
    $user_post = $_POST['id-usuario-logado'];
  }
  if (isset($_POST['tipo-usuario-logado'])) {
    $type_user_post = $_POST['tipo-usuario-logado'];
  }
  if (isset($_POST['titulo-link-externo-post'])) {
    $titulo_link_externo = $_POST['titulo-link-externo-post'];
  }
  if (isset($_POST['url-link-externo-post'])) {
    $url_link_externo = $_POST['url-link-externo-post'];
  }

  //Edita post
  if (isset($_POST['id_post'])) {
    $id_post = $_POST['id_post'];
  }

  //Login
  if (isset($_POST['login-user'])) {
    $login_user = $_POST['login-user'];
  }

  if (isset($_POST['login-pass'])) {
    $login_pass = $_POST['login-pass'];
  }

  //Operacoes
  if (isset($_POST['nome-nova-operacao'])) {
    $nome_nova_operacao = $_POST['nome-nova-operacao'];
  }

  if (isset($_POST['slug-nova-operacao'])) {
    $slug_nova_operacao = $_POST['slug-nova-operacao'];
  }

  if (isset($_POST['tipo-nova-operacao'])) {
    $tipo_nova_operacao = $_POST['tipo-nova-operacao'];
  }

  //Gestor
  if (isset($_POST['nome-novo-gestor'])) {
    $nome_novo_gestor = $_POST['nome-novo-gestor'];
  }

  if (isset($_POST['operacao-novo-gestor'])) {
    $operacao_novo_gestor = $_POST['operacao-novo-gestor'];
  }

  if (isset($_POST['email-novo-gestor'])) {
    $email_novo_gestor = $_POST['email-novo-gestor'];
  }

  //Usuário
  if (isset($_POST['nome-novo-usuario'])) {
    $nome_novo_usuario = $_POST['nome-novo-usuario'];
  }

  if (isset($_POST['login-novo-usuario'])) {
    $login_novo_usuario = $_POST['login-novo-usuario'];
  }

  if (isset($_POST['email-novo-usuario'])) {
    $email_novo_usuario = $_POST['email-novo-usuario'];
  }

  if (isset($_POST['nova-senha-usuario'])) {
    $nova_senha = $_POST['nova-senha-usuario'];
  }

  //Excluir
  if (isset($_POST['id_excluir'])) {
    $id_excluir = $_POST['id_excluir'];
  }

  if (isset($_POST['item_excluir'])) {
    $item_excluir = $_POST['item_excluir'];
  }

  //Alterar campos para usuario
  if (isset($_POST['id_altera_nome_usuario'])) {
    $id_altera_nome_usuario = $_POST['id_altera_nome_usuario'];
  }

  if (isset($_POST['id_altera_tipo_usuario'])) {
    $id_altera_tipo_usuario = $_POST['id_altera_tipo_usuario'];
  }

  if (isset($_POST['id_altera_email_usuario'])) {
    $id_altera_email_usuario = $_POST['id_altera_email_usuario'];
  }

  if (isset($_POST['id_altera_login_usuario'])) {
    $id_altera_login_usuario = $_POST['id_altera_login_usuario'];
  }

  if (isset($_POST['id_altera_status_usuario'])) {
    $id_altera_status_usuario = $_POST['id_altera_status_usuario'];
  }

  if (isset($_POST['altera_nome_usuario'])) {
    $altera_nome_usuario = $_POST['altera_nome_usuario'];
  }

  if (isset($_POST['altera_email_usuario'])) {
    $altera_email_usuario = $_POST['altera_email_usuario'];
  }

  if (isset($_POST['altera_tipo_usuario'])) {
    $altera_tipo_usuario = $_POST['altera_tipo_usuario'];
  }

  if (isset($_POST['altera_login_usuario'])) {
    $altera_login_usuario = $_POST['altera_login_usuario'];
  }

  if (isset($_POST['altera_status_usuario'])) {
    $altera_status_usuario = $_POST['altera_status_usuario'];
  }

  //Alterar campos para gestores
  if (isset($_POST['id_altera_nome_gestor'])) {
    $id_altera_nome_gestor = $_POST['id_altera_nome_gestor'];
  }

  if (isset($_POST['id_altera_operacao_gestor'])) {
    $id_altera_operacao_gestor = $_POST['id_altera_operacao_gestor'];
  }

  if (isset($_POST['id_altera_email_gestor'])) {
    $id_altera_email_gestor = $_POST['id_altera_email_gestor'];
  }

  if (isset($_POST['altera_nome_gestor'])) {
    $altera_nome_gestor = $_POST['altera_nome_gestor'];
  }

  if (isset($_POST['altera_email_gestor'])) {
    $altera_email_gestor = $_POST['altera_email_gestor'];
  }

  if (isset($_POST['altera_operacao_gestor'])) {
    $altera_operacao_gestor = $_POST['altera_operacao_gestor'];
  }

  //Alterar campos para operacoes
  if (isset($_POST['id_altera_nome_operacao'])) {
    $id_altera_nome_operacao = $_POST['id_altera_nome_operacao'];
  }

  if (isset($_POST['altera_nome_operacao'])) {
    $altera_nome_operacao = $_POST['altera_nome_operacao'];
  }


  //Trigger das funções chamadas via AJAX
  if(isset($acao)){
    switch($acao) {
      case "login": {
        login($PDO, $login_user, $login_pass); // Login
      };
      break;

      case "alteraSenhaPrimeiroAcesso": {
        alteraSenhaPrimeiroAcesso($PDO, $nova_senha, $login_pass) ; // Altera senha primeiro acesso
      };
      break;

      case "listaPostsPorOperacao": {
        listaPostsPorOperacao($PDO, $operacao); // Lista Post por Operacao --> Tela de Publicacoes
      };
      break;

      case "listaTodosPosts": {
        listaTodosPosts($PDO); // Lista Post por Operacao --> Tela de Publicacoes
      };
      break;

      case "incluiPost": {
        incluiPost($PDO, $user_post, $titulo_link_externo, $url_link_externo, $titulo_post, $imagem_post, $texto_post, $novo_post_operacoes, $inp_box_pos, $post_status); // Novo post --> Tela de Publicacoes
      };
      break;

      case "editaPost": {
        editaPost($PDO, $id_post); // Edita post --> Tela de Publicacoes
      };
      break;

      case "editaPublicaPost": {
        editaPublicaPost($PDO, $id_post, $user_post, $titulo_link_externo, $url_link_externo, $titulo_post, $imagem_post, $texto_post, $novo_post_operacoes, $inp_box_pos, $post_status); // Edita post --> Tela de Publicacoes
      };
      break;

      case "listaGestoresPorOperacao": {
        listaGestoresPorOperacao($PDO, $operacao); // Lista Post por Operacao --> Tela de Publicacoes
      };
      break;

      case "incluirOperacao": {
        incluirOperacao($PDO, $nome_nova_operacao, $slug_nova_operacao, $tipo_nova_operacao); // Inclui Operacao --> Tela de Publicacoes
      };
      break;

      case "alteraNomeOperacoes": {
        alteraNomeOperacoes($PDO, $altera_nome_operacao, $id_altera_nome_operacao); // Altera nome da operacao
      };
      break;

      case "alteraNomeOperacao": {
        alteraNomeOperacao($PDO, $altera_nome_operacao, $id_altera_nome_operacao); // Altera Nome da Operacao --> Tela de Publicacoes
      };
      break;

      case "incluirGestor": {
        incluirGestor($PDO, $nome_novo_gestor, $operacao_novo_gestor, $email_novo_gestor); // Inclui Gestor --> Tela de Publicacoes
      };
      break;

      case "alteraNomeGestor": {
        alteraNomeGestor($PDO, $altera_nome_gestor, $id_altera_nome_gestor); // Altera Nome do Gestor --> Tela de Publicacoes
      };
      break;

      case "alteraEmailGestor": {
        alteraEmailGestor($PDO, $altera_email_gestor, $id_altera_email_gestor); // Altera Email do Gestor --> Tela de Publicacoes
      };
      break;

      case "alteraOperacaoGestor": {
        alteraOperacaoGestor($PDO, $altera_operacao_gestor, $id_altera_operacao_gestor); // Altera Operacao do Gestor --> Tela de Publicacoes
      };
      break;

      case "editaGestor": {
        editaGestor($PDO, $gestor); // Lista Post por Operacao --> Tela de Publicacoes
      };
      break;

      case "incluirUsuario": {
        incluirUsuario($PDO, $nome_novo_usuario, $email_novo_usuario, $login_novo_usuario); // Inclui usuário --> Tela de Publicacoes
      };
      break;

      case "alteraNomeUsuario": {
        alteraNomeUsuario($PDO, $altera_nome_usuario, $id_altera_nome_usuario); // Altera usuário --> Tela de Publicacoes
      };
      break;

      case "alteraTipoUsuario": {
        alteraTipoUsuario($PDO, $altera_tipo_usuario, $id_altera_tipo_usuario); // Altera usuário --> Tela de Publicacoes
      };
      break;

      case "alteraLoginUsuario": {
        alteraLoginUsuario($PDO, $altera_login_usuario, $id_altera_login_usuario); // Altera login --> Tela de Publicacoes
      };
      break;

      case "alteraEmailUsuario": {
        alteraEmailUsuario($PDO, $altera_email_usuario, $id_altera_email_usuario); // Altera login --> Tela de Publicacoes
      };
      break;

      case "alteraStatusUsuario": {
        alteraStatusUsuario($PDO, $altera_status_usuario, $id_altera_status_usuario); // Altera status usuário --> Tela de Publicacoes
      };
      break;

      case "excluir": {
        excluir($PDO, $item_excluir, $id_excluir); // Exclusao geral --> Tela de Publicacoes
      };
      break;
    }
  }




/* =-=-=-= = = = - - Funções - - = = = =-=-=-= */


///// *LOGIN /////
function login($PDO, $login_user, $login_pass) {
  $sql = "SELECT * from tb_usuarios
          WHERE login_usuario='$login_user'
          AND senha_usuario='$login_pass'
          AND status_usuario = 1";

  $result = $PDO->query($sql);
  $rows = $result->fetchAll();
  $count = $result->rowCount();

  $primeiro_acesso = $rows[0]['primeiro_acesso'];

  if($count == "1") {
    $_SESSION['login'] = "yes";
    foreach($rows as $username){
       $_SESSION['nome_usuario'] = $username['nome_usuario'];
       $_SESSION['id_usuario'] = $username['id_usuario'];
       $_SESSION['tipo_usuario'] = $username['tipo_usuario'];
    }
    echo $count;
  } else {
    $_SESSION['login'] = "no";
    echo $count;
    session_destroy();
  }
}


///// *EXCLUIR /////
function excluir($PDO, $item_excluir, $id_excluir) {
  echo $item_excluir;
  echo 'id='.$item_excluir;
  switch($item_excluir) {
    case "post": {
      $delete = "DELETE FROM tb_posts WHERE id_post='$id_excluir'";
    };
    break;

    case "gestor": {
      $delete = "DELETE FROM tb_gestores WHERE id_gestor='$id_excluir'";
    };
    break;

    case "usuario": {
      $delete = "DELETE FROM tb_usuarios WHERE id_usuario='$id_excluir'";
    };
    break;

    case "operacao": {
      $delete = "DELETE FROM tb_operacoes WHERE id_operacao='$id_excluir'";
    };
    break;
  }

  echo $delete;

  $stmt = $PDO->prepare($delete);

  $stmt->execute();

  echo 'ok';
}



///// *OPERAÇÕES /////

//Select Dropdown com a lista de operacoes
function operacoesLista($PDO) {
  $sql = "SELECT * FROM tb_operacoes";
  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  $i = 0;
  $opeList = array();

  echo "<div class='container-select-lista-operacoes'>";
    echo "<label>Selecione a operação</label>";
    echo "<div class='container-simple-select'>";
      echo "<span class='trigger-simple-select'>Selecione...</span>";
      echo "<div class='row'>";
        echo "<ul id='simple-select' class='simple-select lista-operacoes'>";
        foreach($rows as $opRet) {
          $opeList['id_operacao'][$i] = $opRet['id_operacao'];
          $opeList['nome_operacao'][$i] = $opRet['nome_operacao'];
          $opeList['slug_operacao'][$i] = $opRet['slug_operacao'];
          echo '<li id='.$opeList['id_operacao'][$i].' rel='.$opeList['slug_operacao'][$i].'>'.$opeList['nome_operacao'][$i].'</li>';
          $i++;
        }
        echo "</ul>";
      echo "</div>";
      echo "<input type='hidden' name='operacao' class='hidden-operacao' />";
    echo "</div>";
  echo "</div>";
}

// Exibe as operações como tabela
function listaOperacoesTabela($PDO) {
  $sql = "SELECT * FROM tb_operacoes o
          INNER JOIN tb_gestores g
          ON o.id_gestor_responsavel=g.id_gestor";
  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  echo "
    <div class='row mt30'>
      <table class='table-admin tabela-gestores'>
        <thead>
          <tr>
            <th>
              <i class='fa fa-users mr5' aria-hidden='true'></i>Operação
            </th>
            <th>
              <i class='fa fa-user mr5' aria-hidden='true'></i>Gestor
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody>";
  foreach($rows as $operacao) {
      echo "
          <tr>
            <td>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <div class='left wd30'>
                <div class='container-btn-tip' id='tip-altera-campos'>
                  <div class='btn-tip'>
                    <div class='trigger-btn-tip btn-tip-lista-gestores left'>
                      <i class='fa fa-question-circle' aria-hidden='true'></i>
                    </div>
                    <div class='bubble-tip'>
                      <div class='bubble-tip-icon'>
                        <i class='fa fa-exclamation' aria-hidden='true'></i>
                      </div>
                      <div class='bubble-tip-text'>
                        Clique no <b>campo para</b> para editá-lo.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class='left wd200'>
                <input type='text' class='input-table-ajax input-altera-nome-operacao' name='altera-nome-operacao' rel='altera-nome-operacao' id='".$operacao['id_operacao']."' placeholder='".$operacao['nome_operacao']."' value='".$operacao['nome_operacao']."' />
              </div>
            </td>
            <td>".$operacao['nome_gestor']."</td>
            <td>
              <button type='button' id='".$operacao['id_operacao']."' class='master btn-exclui-inline btn-exclui-operacao' rel='btn-exclui-operacao' title='Excluir essa operação' alt='Excluir essa operação'><i class='fa fa-times-circle ' aria-hidden='true'></i></button>
            </td>
          </tr>
       ";
  }
  echo "</tbody>
      </table>
    </div>";
}


//Lista Operacoes simples
function listaOperacoes($PDO){
  $sql = "SELECT * FROM tb_operacoes";
  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  return json_encode($rows);

  foreach($rows as $operacoes) {
      echo $operacoes['nome_operacao'];
  }
}

//Incluir Operacao
function incluirOperacao($PDO, $nome_nova_operacao, $slug_nova_operacao, $tipo_nova_operacao) {
  $sql = "INSERT INTO tb_operacoes (
    nome_operacao,
    slug_operacao,
    tipo_operacao
    )
  VALUES(
    '".$nome_nova_operacao."',
    '".$slug_nova_operacao."',
    '".$tipo_nova_operacao."'
  )";

  $stmt = $PDO->prepare($sql);

  $stmt->execute();

  echo "ok";
}

//Altera Nome de Operacoes alteraNomeOperacoes()
function alteraNomeOperacao($PDO, $altera_nome_operacao, $id_altera_nome_operacao) {
  $sqlUpd = "UPDATE tb_operacoes SET
    nome_operacao = '$altera_nome_operacao'
    WHERE id_operacao = '$id_altera_nome_operacao'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}




///// *USUÁRIOS /////

//Inclui usuario
function incluirUsuario($PDO, $nome_novo_usuario, $email_novo_usuario, $login_novo_usuario) {
  $data_criacao_usuario = date('Y-m-d');
  $sql = "INSERT INTO tb_usuarios (
    nome_usuario,
    email_usuario,
    login_usuario,
    senha_usuario,
    primeiro_acesso,
    status_usuario,
    tipo_usuario,
    data_criacao_usuario
    )
  VALUES(
    '".$nome_novo_usuario."',
    '".$email_novo_usuario."',
    '".$login_novo_usuario."',
    '1234',
    '0',
    '1',
    '1',
    '".$data_criacao_usuario."'
  )";

  echo $sql;

  $stmt = $PDO->prepare($sql);

  $stmt->execute();

  echo "ok";
}

//Altera Tipo de Usuario alteraTipoUsuario()
function alteraTipoUsuario($PDO, $altera_tipo_usuario, $id_altera_tipo_usuario) {
  $sqlUpd = "UPDATE tb_usuarios SET
    tipo_usuario = '$altera_tipo_usuario'
    WHERE id_usuario = '$id_altera_tipo_usuario'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Altera Nome de Usuario alteraNomeUsuario()
function alteraNomeUsuario($PDO, $altera_nome_usuario, $id_altera_nome_usuario) {
  $sqlUpd = "UPDATE tb_usuarios SET
    nome_usuario = '$altera_nome_usuario'
    WHERE id_usuario = '$id_altera_nome_usuario'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Altera Login de Usuario alteraNomeUsuario()
function alteraLoginUsuario($PDO, $altera_login_usuario, $id_altera_login_usuario) {
  $sqlUpd = "UPDATE tb_usuarios SET
    login_usuario = '$altera_login_usuario'
    WHERE id_usuario = '$id_altera_login_usuario'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Altera Email de Usuario alteraNomeUsuario()
function alteraEmailUsuario($PDO, $altera_email_usuario, $id_altera_email_usuario) {
  $sqlUpd = "UPDATE tb_usuarios SET
    email_usuario = '$altera_email_usuario'
    WHERE id_usuario = '$id_altera_email_usuario'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Altera Status de Usuario alteraStatusUsuario()
function alteraStatusUsuario($PDO, $altera_status_usuario, $id_altera_status_usuario) {
  $sqlUpd = "UPDATE tb_usuarios SET
    status_usuario = '$altera_status_usuario'
    WHERE id_usuario = '$id_altera_status_usuario'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Altera senha primeiro acesso
function alteraSenhaPrimeiroAcesso($PDO, $nova_senha, $login_pass) {
  $sql = "SELECT * from tb_usuarios
          WHERE login_usuario='$login_user'
          AND senha_usuario='$login_pass'";

  $result = $PDO->query($sql);
  $count = $result->rowCount();

  if($count > 0) {
    $sqlUpd = "UPDATE tb_usuarios SET
      senha_usuario = '$nova_senha',
      status_usuario = '1',
      WHERE login_usuario = '$login_pass'
    ";

    $stmt = $PDO->prepare($sqlUpd);

    echo $sqlUpd;

    $stmt = $PDO->prepare($sqlUpd);

    $stmt->execute();

    echo "ok";
  } else {
    die("erro");
  }
}

//Lista Usuários
function listaUsuarios($PDO){
  $sql = "SELECT * FROM tb_usuarios
        INNER JOIN tb_gestores
        ON tb_gestores.id_gestor=tb_usuarios.id_gestor_usuario
        INNER JOIN tb_status
        ON tb_status.id_status=tb_usuarios.status_usuario
        INNER JOIN tb_operacoes
        ON tb_operacoes.id_operacao=tb_usuarios.id_operacao_usuario";
  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  echo "<ul class='lista-usuarios'>";
  foreach($rows as $usuarios) {
    echo "<li rel='".$usuarios['login_usuario']."'>Nome: ".$usuarios['nome_usuario']." - Operação: ".$usuarios['nome_operacao']." - Gestor: ".$usuarios['nome_gestor']."</li>";
  }
  echo "</ul>";
}

//Lista todos os usuarios
function listaTodosUsuarios($PDO){
  $sql = "SELECT * FROM tb_usuarios u
          INNER JOIN tb_tipo_usuario t
          ON t.id_tipo_usuario=u.tipo_usuario
          INNER JOIN tb_status s
          ON s.id_status=u.status_usuario
          ORDER BY nome_usuario ASC";
  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  echo "
    <div class='row mt30'>
      <table class='table-admin tabela-gestores'>
        <thead>
          <tr>
            <th>
              <i class='fa fa-star mr5' aria-hidden='true'></i>Tipo
            </th>
            <th>
              <i class='fa fa-user mr5' aria-hidden='true'></i>Nome
            </th>
            <th>
              <i class='fa fa-sign-in mr5' aria-hidden='true'></i>Login
            </th>
            <th>
              <i class='fa fa-envelope mr5' aria-hidden='true'></i>E-mail
            </th>
            <th>
              <i class='fa fa-thumbs-up mr5' aria-hidden='true'></i>Status
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody>";
  foreach($rows as $usuarios) {
      echo "
          <tr>
            <td class='td-tipo-usuario wd80'>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <span class='btn-trigger-altera-tipo-usuario'>".$usuarios['tipo_usuario']."</span>
              <ul class='container-lista-altera-tipo-usuario'>
                <input type='hidden' class='input-table-ajax input-altera-tipo-usuario' name='altera-tipo-usuario' rel='altera-tipo-usuario' id='".$usuarios['id_usuario']."' value='".$usuarios['tipo_usuario']."' />
                <li rel='admin' id='1'>
                  Admin
                </li>
                <li rel='editor' id='3'>
                  Editor
                </li>
                <li rel='usuario' id='4'>
                  Usuário
                </li>
              </ul>
            </td>
            <td>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <div class='left mr10 wd10per'>
                <div class='container-btn-tip' id='tip-altera-campos'>
                  <div class='btn-tip'>
                    <div class='trigger-btn-tip btn-tip-lista-gestores left'>
                      <i class='fa fa-question-circle' aria-hidden='true'></i>
                    </div>
                    <div class='bubble-tip'>
                      <div class='bubble-tip-icon'>
                        <i class='fa fa-exclamation' aria-hidden='true'></i>
                      </div>
                      <div class='bubble-tip-text'>
                        Clique no <b>campo para</b> para editá-lo.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class='right wd85per'>
                <input type='text' class='input-table-ajax input-altera-nome-usuario' name='altera-nome-usuario' rel='altera-nome-usuario' id='".$usuarios['id_usuario']."' placeholder='".$usuarios['nome_usuario']."' value='".$usuarios['nome_usuario']."' />
              </div>
            </td>
            <td>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <div class='right wd80'>
                <input type='text' class='input-table-ajax input-altera-login-usuario' name='altera-login-usuario' rel='altera-login-usuario' id='".$usuarios['id_usuario']."' placeholder='".$usuarios['login_usuario']."' value='".$usuarios['login_usuario']."' />
              </div>
            </td>
            <td>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <div class='right wd230'>
                <input type='text' class='input-table-ajax input-altera-email-usuario' name='altera-email-usuario' rel='altera-email-usuario' id='".$usuarios['id_usuario']."' placeholder='".$usuarios['email_usuario']."' value='".$usuarios['email_usuario']."' />
              </div>
            </td>
            <td class='td-status-usuario wd100'>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <span class='btn-trigger-altera-status-usuario ".$usuarios['tipo_status']."'><span class='bullet-status-post'></span>".$usuarios['tipo_status']."</span>
              <ul class='container-lista-altera-status-usuario'>
                <input type='hidden' class='input-table-ajax input-altera-status-usuario' name='altera-status-usuario' rel='altera-status-usuario' id='".$usuarios['id_usuario']."' value='".$usuarios['status_usuario']."' />
                <li rel='ativo' id='1' class='ativo'>
                  <span class='bullet-status-post'></span> Ativo
                </li>
                <li rel='inativo' id='2' class='inativo'>
                  <span class='bullet-status-post'></span> Inativo
                </li>
              </ul>

            </td>
            <td class='wd50'>
              <button type='button' id='".$usuarios['id_usuario']."' class='btn-exclui-inline btn-exclui-usuario master' rel='btn-exclui-usuario' title='Excluir esse usuário' alt='Excluir esse usuário'><i class='fa fa-times-circle ' aria-hidden='true'></i></button>
            </td>
          </tr>
       ";
  }
  echo "</tbody>
      </table>
    </div>";
}


///// *GESTORES /////

//Lista os Gestores
function listaTodosGestores($PDO){
  $sql = "SELECT * FROM tb_gestores
          INNER JOIN tb_operacoes
          ON tb_operacoes.id_operacao=tb_gestores.id_operacao_gestor";

  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  echo "
    <div class='row mt30'>
      <table class='table-admin tabela-gestores'>
        <thead>
          <tr>
            <th>
              <i class='fa fa-user mr5' aria-hidden='true'></i>Nome
            </th>
            <th>
              <i class='fa fa-users mr5' aria-hidden='true'></i>Operação
            </th>
            <th>
              <i class='fa fa-envelope mr5' aria-hidden='true'></i>E-mail
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody>";
  foreach($rows as $gestores) {
      echo "
          <tr>
            <td class='td-nome-gestor'>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <div class='left wd30'>
                <div class='container-btn-tip' id='tip-altera-campos'>
                  <div class='btn-tip'>
                    <div class='trigger-btn-tip btn-tip-lista-gestores left'>
                      <i class='fa fa-question-circle' aria-hidden='true'></i>
                    </div>
                    <div class='bubble-tip'>
                      <div class='bubble-tip-icon'>
                        <i class='fa fa-exclamation' aria-hidden='true'></i>
                      </div>
                      <div class='bubble-tip-text'>
                        Clique no <b>campo para</b> para editá-lo.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class='left wd200'>
                <input type='text' class='input-table-ajax input-altera-nome-gestor' name='altera-nome-gestor' rel='altera-nome-gestor' id='".$gestores['id_gestor']."' placeholder='".$gestores['nome_gestor']."' value='".$gestores['nome_gestor']."' />
              </div>
            </td>
            <td>".$gestores['nome_operacao']."</td>
            <td>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <div class='left wd230'>
                <input type='text' class='input-table-ajax input-altera-email-gestor' name='altera-email-gestor' rel='altera-email-gestor' id='".$gestores['id_gestor']."' placeholder='".$gestores['email_gestor']."' value='".$gestores['email_gestor']."' />
              </div>
            </td>
            <td>
              <button type='button' id='".$gestores['id_gestor']."' class='btn-exclui-inline btn-exclui-gestor' rel='btn-exclui-gestor' title='Excluir esse gestor' alt='Excluir esse gestor'><i class='fa fa-times-circle ' aria-hidden='true'></i></button>
            </td>
          </tr>
       ";
  }
  echo "</tbody>
      </table>
    </div>";
}

//Incluir Gestor
function incluirGestor($PDO, $nome_novo_gestor, $operacao_novo_gestor, $email_novo_gestor) {
  $sql = "INSERT INTO tb_gestores (
    nome_gestor,
    id_operacao_gestor,
    email_gestor
    )
  VALUES(
    '".$nome_novo_gestor."',
    '".$operacao_novo_gestor."',
    '".$email_novo_gestor."'
  )";

  $stmt = $PDO->prepare($sql);

  $stmt->execute();

  echo "ok";
}


//Altera Nome de Gestor alteraNomeGestor()
function alteraNomeGestor($PDO, $altera_nome_gestor, $id_altera_nome_gestor) {
  $sqlUpd = "UPDATE tb_gestores SET
    nome_gestor = '$altera_nome_gestor'
    WHERE id_gestor = '$id_altera_nome_gestor'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Altera Email de Gestor alteraEmailGestor()
function alteraEmailGestor($PDO, $altera_email_gestor, $id_altera_email_gestor) {
  $sqlUpd = "UPDATE tb_gestores SET
    email_gestor = '$altera_email_gestor'
    WHERE id_gestor = '$id_altera_email_gestor'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Altera Operacao de Gestor alteraOperacaoGestor()
function alteraOperacaoGestor($PDO, $altera_operacao_gestor, $id_altera_operacao_gestor) {
  $sqlUpd = "UPDATE tb_gestores SET
    operacao_gestor = '$altera_operacao_gestor'
    WHERE id_gestor = '$id_altera_operacao_gestor'
  ";

  echo $sqlUpd;
  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();

  echo 'ok';
}

//Editar o gestor
function editaGestor($PDO, $gestor){
  $sql = "SELECT * FROM tb_gestores
          INNER JOIN tb_operacoes
          ON tb_operacoes.id_operacao=tb_gestores.id_operacao_gestor
          WHERE id_gestor = $gestor";

  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  foreach($rows as $gestorSelec) {
    echo $gestorSelec['nome_gestor']." - ".$gestorSelec['nome_operacao'];
  }
}


function listaGestoresPorOperacao($PDO, $operacao){
  if($operacao != "todas"){
    $sql = "SELECT * FROM tb_gestores
            INNER JOIN tb_operacoes
            ON tb_operacoes.id_operacao=$operacao
            WHERE id_operacao_gestor = $operacao";
  } else {
    $sql = "SELECT * FROM tb_gestores
            INNER JOIN tb_operacoes
            ON tb_operacoes.id_operacao=tb_gestores.id_operacao_gestor";
  }

  $result = $PDO->query($sql);
  $rows = $result->fetchAll();

  echo "
    <div class='row mt30'>
      <input type='hidden' class='hidden-nome-gestor' />
      <table class='table-admin tabela-gestores'>
        <thead>
          <tr>
            <th>
              <i class='fa fa-user mr5' aria-hidden='true'></i>Nome
            </th>
            <th>
              <i class='fa fa-users mr5' aria-hidden='true'></i>Operação
            </th>
            <th>
              <i class='fa fa-envelope mr5' aria-hidden='true'></i>E-mail
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody>";
  foreach($rows as $gestores) {
      echo "
          <tr>
            <td>
              <a class='nome-gestor' rel='".$gestores['id_gestor']."'>".$gestores['nome_gestor']."</a>
              <div class='container-btn-tip' id='tip-lista-gestores'>
                <div class='btn-tip'>
                  <div class='trigger-btn-tip btn-tip-lista-gestores left ml10'>
                    <i class='fa fa-question-circle' aria-hidden='true'></i>
                  </div>
                  <div class='bubble-tip'>
                    <div class='bubble-tip-icon'>
                      <i class='fa fa-exclamation' aria-hidden='true'></i>
                    </div>
                    <div class='bubble-tip-text'>
                      Clique no <b>nome do gestor</b> para editá-lo.
                    </div>
                  </div>
                </div>
              </div>
            </td>
            <td>".$gestores['nome_operacao']."</td>
            <td>
              <a class='mail-gestor' href='mailto:".$gestores['email_gestor']."'>".$gestores['email_gestor']."</a>
            </td>
            <td>
              <button type='button' id='".$gestores['id_gestor']."' class='btn-exclui-inline btn-exclui-gestor' rel='btn-exclui-gestor' title='Excluir esse gestor' alt='Excluir esse gestor'><i class='fa fa-times-circle ' aria-hidden='true'></i></button>
            </td>
          </tr>
       ";
  }
  echo "</tbody>
      </table>
    </div>";
}


///// P O S T S /////

//Novo post --> campos estao no topo do arquivo
function incluiPost($PDO, $user_post, $titulo_link_externo, $url_link_externo, $titulo_post, $imagem_post, $texto_post, $novo_post_operacoes, $inp_box_pos, $post_status) {
  $data_criacao_post = date('Y-m-d');

  //colocar o restante dos itens da tabela para registro

  $sql = "INSERT INTO tb_posts (
    titulo_post,
    id_operacoes,
    id_user_post_criador,
    texto_post,
    imagem_post,
    titulo_link_externo,
    url_link_externo,
    post_status,
    box_post,
    data_criacao_post
    )
  VALUES(
    '".$titulo_post."',
    '".$novo_post_operacoes."',
    '".$user_post."',
    '".$texto_post."',
    '".$imagem_post."',
    '".$titulo_link_externo."',
    '".$url_link_externo."',
    '".$post_status."',
    '".$inp_box_pos."',
    '".$data_criacao_post."'
  )";

  $stmt = $PDO->prepare($sql);

  $stmt->execute();

  echo "ok";
}

//Edita post
function editaPost($PDO, $id_post) {
  $sql = "SELECT * FROM tb_posts e
          INNER JOIN tb_usuarios a
          ON e.id_user_post_criador=a.id_usuario
          INNER JOIN tb_status s
          ON e.post_status=s.id_status
          WHERE e.id_post = '$id_post'";

          $result = $PDO->query($sql);
          $rows = $result->fetchAll();

          echo "
                <span class='modal-title'>EDITAR PUBLICAÇÃO<button class='fa fa-close btn-close-modal' aria-hidden='true'></button></span>
                <div class='content-modal'>
                  <form id='form-edita-post' rel='".$rows[0]['id_post']."'>

                    <input type='hidden' class='id-usuario-logado' rel='usuario-que-modificou-post' name='id-usuario-logado' value='".$_SESSION['id_usuario']."' />
                    <input type='hidden' class='tipo-usuario-logado' name='tipo-usuario-logado' value='".$_SESSION['tipo_usuario']."' />
                    <input type='hidden' class='id_post' name='id_post' value='".$rows[0]['id_post']."' />

                    <div class='aba-formulario novo-post-passo-1'>
                      <div class='line full'>
                        <div class='half-col-inp left'>
                          <label>Título da publicação</label>
                          <input type='text' class='inp input-titulo-post' name='titulo-post' placeholder='".$rows[0]['titulo_post']."' value='".$rows[0]['titulo_post']."' />
                        </div>
                        <div class='half-col-inp right select-operacoes-edita-post'>";

                          $sql2 = "SELECT * FROM tb_operacoes";
                          $result2 = $PDO->query($sql2);
                          $rows2 = $result2->fetchAll();

                          $i = 0;
                          $opeList = array();

                      echo "<label>Selecione a operação</label>";
                      echo "<div class='container-simple-select'>";
                        echo "<span class='trigger-simple-select'>Selecione...<i class='fa fa-caret-down' aria-hidden='true'></i></span>";
                        echo "<div class='row'>";
                          echo "<ul id='simple-select' class='simple-select lista-operacoes'>";
                          foreach($rows2 as $opRet) {
                            $opeList['id_operacao'][$i] = $opRet['id_operacao'];
                            $opeList['nome_operacao'][$i] = $opRet['nome_operacao'];
                            $opeList['slug_operacao'][$i] = $opRet['slug_operacao'];
                            echo '<li id='.$opeList['id_operacao'][$i].' rel='.$opeList['slug_operacao'][$i].'>'.$opeList['nome_operacao'][$i].'</li>';
                            $i++;
                          }
                          echo "</ul>";
                        echo "</div>";
                        echo "<input type='hidden' name='operacao' class='hidden-operacao' />";
                      echo "</div>
                        </div>
                      </div>
                      <div class='row operacoes-selecionadas-post' style='display: block!important;'>";

                        $opspl = explode(",", $rows[0]['id_operacoes']);

                        foreach ($opspl as $idOps) {
                          $i = (float)$idOps - 1;

                          if($i == '-1') {
                            $i = '0';
                          }

                          echo "
                            <div tag-operacao='".$idOps."' class='tag-operacao tag-".$opeList['slug_operacao'][$i]."'><span>".$opeList['nome_operacao'][$i]."</span><i class='fa fa-times-circle' aria-hidden='true'></i></div>
                          ";
                        }

                      echo "
                        <input type='hidden' name='novo-post-operacoes' class='novo-post-operacoes' value='".$rows[0]['id_operacoes']."' />
                      </div>
                      <div class='line full mt20'>
                        <div class='left full'>
                          <label><span class='left'>Título da publicação</span><span class='count-chars-textarea'>Restam <span class='chars-count-red'>210</span> caracteres...</span></label>
                          <textarea type='text' class='textarea textarea-texto-post' maxlength='210' name='texto-post' placeholder='Texto da publicação' value='".$rows[0]['texto_post']."'>".$rows[0]['texto_post']."</textarea>
                        </div>
                      </div>

                      <div class='row mt20 ct-btn-novo-post-passo-2'>
                        <input type='hidden' class='valida-edita-input-titulo-post' value='1' />
                        <input type='hidden' class='valida-edita-tags-operacoes' value='1' />
                        <input type='hidden' class='valida-edita-textarea-texto-post' value='1' />

                        <div class='left'>
                          <button type='button' class='btn btn-edit left btn-close-modal-big'><i class='fa fa-arrow-left' aria-hidden='true'></i>FECHAR</button>
                        </div>
                        <div class='right'>
                          <div class='mask-block'></div>
                          <button type='button' class='btn btn-green-ok right btn-steps-modal' id='chkTags' rel='novo-post-passo-2'><i class='fa fa-arrow-right' aria-hidden='true'></i>AVANÇAR</button>
                        </div>
                      </div>
                    </div>

                    <!-- Passo 2 -->
                    <div class='aba-formulario ct-modal-exclui-imagem-post'></div>
                    <div class='aba-formulario novo-post-passo-2'>
                      <div class='line full'>
                        <div class='half-col-inp left'>
                          <div class='row'>
                            <label>Imagem de destaque</label>
                            <textarea name='post_editor' class='textarea post_editor' id='post_editor2'></textarea>
                            <input type='hidden' name='hidden_post_editor' class='hidden_post_editor' value='".$rows[0]['imagem_post']."' />
                          </div>";

                          if($rows[0]['imagem_post'] != "") {
                            echo "
                              <div class='box-img-edit-post'>
                                ".$rows[0]['imagem_post']."
                                <button type='button' alt='Excluir essa imagem' title='Excluir essa imagem' class='btn-exclui-img-post'><i class='fa fa-close' aria-hidden='true'></i></button>
                              </div>
                             ";
                          }

                      echo "
                          <div class='row mt20 only-admin'>
                            <label>Status do post</label>
                            <div class='left mr20'>
                              <div class='left mr10'>
                                <span class='label-no-float left mt5 mr5'>Ativo</span>
                                <input type='radio' class='post-status' name='post-status' value='1' rel='ativo'  />
                              </div>
                            </div>
                            <div class='left'>
                              <div class='left mr10'>
                                <span class='label-no-float left mt5 mr5'>Inativo</span>
                                <input type='radio' class='post-status' name='post-status' value='2' rel='inativo' checked='checked' />
                              </div>
                            </div>
                          </div>
                          <div class='row mt20'>
                            <div class='left mr10'>
                              <label>Link externo - Título</label>
                              <input type='text' class='inp titulo-link-externo-post' name='titulo-link-externo-post' value='".$rows[0]['titulo_link_externo']."' placeholder='".$rows[0]['titulo_link_externo']."' />
                              <label class='mt20'>Link externo - URL(Endereço)</label>
                              <input type='text' class='inp url-link-externo-post' name='url-link-externo-post' value='".$rows[0]['url_link_externo']."' placeholder='".$rows[0]['url_link_externo']."' />
                            </div>
                          </div>
                        </div>
                        <div class='half-col-inp right'>
                          <label>Selecione o box da publicação</label>
                          <div class='header-box-noticia'>
                            <span class='bullet-red'></span>
                            <span class='bullet-yellow'></span>
                            <span class='bullet-green'></span>
                          </div>
                          <div class='container-selec-box-noticia'>
                            <div class='box-pos box-pos-1' rel='1'>
                              <span class='numero-box'>1</span>
                              <i class='fa fa-check-circle box-pos-check' aria-hidden='true'></i>
                            </div>
                            <div class='col-box-pos-right'>
                              <div class='box-pos box-pos-2' rel='2'>
                                <span class='numero-box'>2</span>
                                <i class='fa fa-check-circle box-pos-check' aria-hidden='true'></i>
                              </div>
                              <div class='box-pos box-pos-3' rel='3'>
                                <span class='numero-box'>3</span>
                                <i class='fa fa-check-circle box-pos-check' aria-hidden='true'></i>
                              </div>
                              <div class='box-pos box-pos-4' rel='4'>
                                <span class='numero-box'>4</span>
                                <i class='fa fa-check-circle box-pos-check' aria-hidden='true'></i>
                              </div>
                            </div>
                            <input type='hidden' class='inp-hidden-box-pos' name='inp-box-pos' value='".$rows[0]['box_post']."' />
                          </div>
                        </div>
                      </div>

                      <div class='row mt20'>
                        <button type='button' class='btn btn-edit left btn-steps-modal' rel='novo-post-passo-1'><i class='fa fa-arrow-left' aria-hidden='true'></i>VOLTAR</button>

                        <div class='right ct-btn-publicar-post'>
                          <button type='button' class='btn btn-green-ok right btn-steps-modal btn-publicar-post' id='btn-editar-post'><i class='fa fa-plus' aria-hidden='true'></i>PUBLICAR</button>
                        </div>
                      </div>
                    </div>
                  </form>

                  <div class='aba-formulario result-edita-post'>
                    <div class='mensagem-result-edita-post mensagem-result-edita-post-enviando'>
                      <div class='left mr30 ct-msg-icon-edita-post'>
                        <i class='fa fa-clipboard icon-msg-post icon-publicando' aria-hidden='true'></i>
                      </div>
                      <div class='left ct-msg-txt-edita-post'>
                        <span class='msg-edita-post'>Publicando... Por favor, aguarde.</span>
                      </div>
                    </div>
                    <div class='mensagem-result-edita-post mensagem-result-edita-post-sucesso'>
                      <div class='left mr30 ct-msg-icon-edita-post'>
                        <i class='fa fa-check icon-msg-post icon-publicado' aria-hidden='true'></i>
                      </div>
                      <div class='left ct-msg-txt-edita-post'>
                        <span class='msg-edita-post'>Pronto! Sua publicação foi enviada com sucesso!</span>
                      </div>
                    </div>
                    <div class='mensagem-result-edita-post mensagem-result-edita-post-erro'>
                      <div class='left mr30 ct-msg-icon-edita-post'>
                        <i class='fa fa-frown-o icon-msg-post icon-erro' aria-hidden='true'></i>
                      </div>
                      <div class='left ct-msg-txt-edita-post'>
                        <span class='msg-edita-post'>Deu um erro ao publicar. Caso continue, <a title='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' alt='Em caso de erro de sistema, por favor, envie um e-mail para sssantos@stefanini.com' href='mailto:sssantos@stefanini.com'>clique aqui</a>.</span>
                      </div>
                    </div>
                  </div>
                </div>
          ";
}

//Publica as alteracoes do post editaPublicaPost()
function editaPublicaPost($PDO, $id_post, $user_post, $titulo_link_externo, $url_link_externo, $titulo_post, $imagem_post, $texto_post, $novo_post_operacoes, $inp_box_pos, $post_status) {
  $sqlUpd = "UPDATE tb_posts SET
    titulo_post = '$titulo_post',
    id_operacoes = '$novo_post_operacoes',
    id_user_ultima_alteracao = '$user_post',
    texto_post = '$texto_post',
    imagem_post = '$imagem_post',
    titulo_link_externo = '$titulo_link_externo',
    url_link_externo = '$url_link_externo',
    post_status = '$post_status',
    box_post = '$inp_box_pos',
    data_ultima_alteracao = CURRENT_TIMESTAMP
    WHERE id_post = '$id_post'
  ";

  $stmt = $PDO->prepare($sqlUpd);
  $stmt->execute();
  echo "ok";
}


//Lista os posts conforme a operacao selecionada
function listaPostsPorOperacao($PDO, $operacao){
/*
  Status do post:
  ativo, inativo e pendente(em aprovacao)
*/
  $sql = "SELECT * FROM tb_posts e
          INNER JOIN tb_usuarios a
          ON e.id_user_post_criador=a.id_usuario
          INNER JOIN tb_operacoes o
          ON o.id_operacao=$operacao
          INNER JOIN tb_status s
          ON e.post_status=s.id_status
          WHERE e.id_operacoes LIKE '%$operacao,%'
          ORDER BY data_criacao_post DESC";

  $result = $PDO->query($sql);
  $rows = $result->fetchAll();
  $count = $result->rowCount();

  if($count > 0){
    echo "
    <div class='row mt30'>
      <table class='table-admin tabela-posts'>
        <thead>
          <tr>
            <th>
              <i class='fa fa-quote-left mr5' aria-hidden='true'></i>Título
            </th>
            <th>
              <i class='fa fa-user mr5' aria-hidden='true'></i>Autor
            </th>
            <th>
              <i class='fa fa-users mr5' aria-hidden='true'></i>Operação
            </th>
            <th>
              <i class='fa fa-calendar mr5' aria-hidden='true'></i>Data
            </th>
            <th>
              <i class='fa fa-thumbs-up mr5' aria-hidden='true'></i>Status
            </th>
            <th></th>
          </tr>
        </thead>
        <tbody>";
    foreach($rows as $posts) {
      $ops = trim($posts['id_operacoes']);
      $ooo = explode(",", $ops);
      array_pop($ooo);
      $count = count($ooo);

      echo "
          <tr>
            <td>
              <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
              <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
              <div class='left wd30'>
                <div class='container-btn-tip' id='tip-altera-campos'>
                  <div class='btn-tip'>
                    <div class='trigger-btn-tip btn-tip-lista-gestores left'>
                      <i class='fa fa-question-circle' aria-hidden='true'></i>
                    </div>
                    <div class='bubble-tip'>
                      <div class='bubble-tip-icon'>
                        <i class='fa fa-exclamation' aria-hidden='true'></i>
                      </div>
                      <div class='bubble-tip-text'>
                        Clique no <b>campo para</b> para editá-lo.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class='left wd100'>
                <a id='".$posts['id_post']."' class='row content-post-admin btn-trigger-title-post-table'>
                  ".$posts['titulo_post']."
                </a>
              </div>
            </td>
            <td>
              ".$posts['nome_usuario']."
            </td>
            <td class='td-nome-operacao'>";
              foreach ($ooo as $lolo) {
                $sql2 = "SELECT * FROM tb_operacoes WHERE id_operacao = '".$lolo."'";
                $result2 = $PDO->query($sql2);
                $rows2 = $result2->fetchAll();

                foreach($rows2 as $opName) {
                  if($count <= 1){
                    echo $opName['nome_operacao'];
                  } else {
                    echo $opName['nome_operacao']." / ";
                  }
                }
              }
      echo "
            </td>
            <td>
              ".date("d/m/Y", strtotime($posts['data_criacao_post']))."
            </td>
            <td class='td-post-status ".$posts['tipo_status']."'>
              <span class='bullet-status-post'></span>".$posts['tipo_status']."
            </td>
            <td>
              <button type='button' id='".$posts['id_post']."' class='btn-exclui-inline btn-exclui-post' rel='btn-exclui-post' title='Excluir esse post' alt='Excluir esse post'><i class='fa fa-times-circle ' aria-hidden='true'></i></button>
            </td>
          </tr>";
    }
    echo "
        </tbody>
      </table>
    </div>";
  } else {
    echo "<span class='warning-msg row mt10'><i class='fa fa-frown-o mr15' aria-hidden='true'></i>Não existem publicações para a operação selecionada</span>";
  }
}

//Lista todos posts
function listaTodosPosts($PDO){
/*
  Status do post:
  ativo, inativo e pendente(em aprovacao)
*/
  $sql = "SELECT * FROM tb_posts p
          INNER JOIN tb_usuarios u
          ON p.id_user_post_criador=u.id_usuario
          INNER JOIN tb_status s
          ON p.post_status=s.id_status
          ORDER BY data_criacao_post DESC";

  $result = $PDO->query($sql);
  $rows = $result->fetchAll();
  $count = $result->rowCount();

  if($count > 0) {
    echo "
      <div class='row mt30'>
        <table class='table-admin tabela-posts'>
          <thead>
            <tr>
              <th>
                <i class='fa fa-quote-left mr5' aria-hidden='true'></i>Título
              </th>
              <th>
                <i class='fa fa-user mr5' aria-hidden='true'></i>Autor
              </th>
              <th>
                <i class='fa fa-users mr5' aria-hidden='true'></i>Operação
              </th>
              <th>
                <i class='fa fa-calendar mr5' aria-hidden='true'></i>Data
              </th>
              <th>
                <i class='fa fa-thumbs-up mr5' aria-hidden='true'></i>Status
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>";
      foreach($rows as $posts) {
        $ops = trim($posts['id_operacoes']);
        $ooo = explode(",", $ops);
        array_pop($ooo);
        $count = count($ooo);

        echo "
            <tr>
              <td class='wd150'>
                <div class='loader-input'><i class='fa fa-spinner' aria-hidden='true'></i></div>
                <div class='loader-ok'><i class='fa fa-thumbs-up' aria-hidden='true'></i></div>
                <div class='left'>
                  <div class='container-btn-tip' id='tip-altera-post'>
                    <div class='btn-tip'>
                      <div class='trigger-btn-tip btn-tip-lista-gestores left'>
                        <i class='fa fa-question-circle' aria-hidden='true'></i>
                      </div>
                      <div class='bubble-tip'>
                        <div class='bubble-tip-icon'>
                          <i class='fa fa-exclamation' aria-hidden='true'></i>
                        </div>
                        <div class='bubble-tip-text'>
                          Clique no <b>título para</b> para editar a publicação.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class='right overflow-title-post-table'>
                  <a id='".$posts['id_post']."' class='content-post-admin btn-trigger-title-post-table'>
                    ".$posts['titulo_post']."
                  </a>
                </div>
              </td>
              <td>
                ".$posts['nome_usuario']."
              </td>
              <td class='td-nome-operacao'>";
                foreach ($ooo as $lolo) {
                  $sql2 = "SELECT * FROM tb_operacoes WHERE id_operacao = '".$lolo."'";
                  $result2 = $PDO->query($sql2);
                  $rows2 = $result2->fetchAll();

                  foreach($rows2 as $opName) {
                    if($count <= 1){
                      echo $opName['nome_operacao'];
                    } else {
                      echo $opName['nome_operacao']." / ";
                    }
                  }
                }
        echo "
              </td>
              <td>
                ".date("d/m/Y", strtotime($posts['data_criacao_post']))."
              </td>
              <td class='td-post-status ".$posts['tipo_status']."'>
                <span class='bullet-status-post'></span>".$posts['tipo_status']."
              </td>
              <td>
                <button type='button' id='".$posts['id_post']."' class='btn-exclui-inline btn-exclui-post' rel='btn-exclui-post' title='Excluir esse post' alt='Excluir esse post'><i class='fa fa-times-circle ' aria-hidden='true'></i></button>
              </td>
            </tr>";
      }
      echo "
          </tbody>
        </table>
      </div>";
  } else {
    echo "<span class='warning-msg row mt10'><i class='fa fa-frown-o mr15' aria-hidden='true'></i>Não existem publicações para a operação selecionada</span>";
  }
}

?>
