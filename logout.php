<?php
  session_start();
  $nome = $_SESSION['nome_usuario'];
  session_destroy();
  include "partials/head.php";
?>

  <body class="login carregando-<?php echo rand(1, 6); ?>">
    <!-- Loader -->
    <?php include "partials/loader.php"; ?>

    <!-- Site Container -->
    <div class="site">
      <!-- Loader -->

      <!-- Home -->
      <section class="container-content">
        <div class="container">

          <!-- Main content -->
          <div class="main-content">
            <div class="container-login">
              <div class="row">
                <img src="src/img/logo-daily.png" class="logo-login" />
                <p class='logout-msg'>
                  <i class="fa fa-frown-o" aria-hidden="true"></i><br /><br />
                  Saindo do <b>Daily News</b>, <br /><?php echo $nome; ?>?<a href="login"><br /><br />Clique aqui</a> entrar novamente.
                </p>
              </div>

              <div class="row mt50">
              <div class="row mt50"></div>
                <img src="src/img/logo-orbitall-branco.png" class="logo-login" />
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Footer -->
      <?php include "partials/footer.php"; ?>
    </div>
    <!-- Site Container -->
    <script>
      $(document).ready(function(){
        setTimeout(function(){
          $(".logout-msg").html("").append("<i class='fa fa-rocket' aria-hidden='true'></i><br /><br />Sim, é um foguete. <br /><br />Que tal fazer o <a href='login'>login</a> agora? Não?");
          setTimeout(function(){
            $(".logout-msg").html("").append("<i class='fa fa-pie-chart' aria-hidden='true'></i><br /><br />Então tá! Esse gráfico mostra o tempo que estamos perdendo aqui esperando... <br /><br /><a href='login'>Clique aqui</a> para resolver.");
            setTimeout(function(){
              $(".logout-msg").html("").append("<i class='fa fa-hand-paper-o' aria-hidden='true'></i><br /><br />Tudo bem... <br />É hora de dizer adeus!");
              setTimeout(function(){
                window.location = "login";
              }, 5000);
            }, 15000);
          }, 20000);
        }, 25000);
      });
    </script>
  </body>
</html>
