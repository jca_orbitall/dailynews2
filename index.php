<?php include "partials/head.php";

  $operacao = isset($_GET['page']);
  $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $operacoes = explode("/", $actual_link);
  $operacao = $operacoes[4];

  $op_orbitall = '5';
  $op_nome = '';

  $sql = "SELECT * FROM tb_operacoes
          WHERE slug_operacao = '$operacao'";

  $result = $PDO->query($sql);
  $rows = $result->fetchAll();
  $count = $result->rowCount();

  foreach($rows as $idOpe) {
    $id_operacao = $idOpe['id_operacao'];
    $op_nome = $idOpe['nome_operacao'];
  }

  if($count == "0"){
    $id_operacao = $op_orbitall;
  }
?>

  <body>
    <!-- Loader -->
    <?php include "partials/loader.php"; ?>

    <!-- Site Container -->
    <div class="site">
      <!-- Loader -->
      <?php include "partials/header.php"; ?>

      <!-- Home -->
      <section class="container-content">
        <div class="container">
          <!-- Content header -->
          <?php //include "partials/header_content.php"; ?>

          <!-- Main content -->
          <div class="main-content">
            <article class="box-main-news box1-<?php echo rand(1, 6); ?>">
              <?php
                if($operacao != "" && $count != "0"){
                  $sql = "SELECT * FROM tb_posts
                          WHERE id_operacoes LIKE '%$id_operacao,%'
                          AND post_status = '1'
                          AND box_post = '1'
                          ORDER BY data_criacao_post DESC
                          LIMIT 1";
                } else {
                  $sql = "SELECT * FROM tb_posts
                          WHERE id_operacoes LIKE '%$id_operacao,%'
                          AND post_status = '1'
                          AND box_post = '1'
                          ORDER BY data_criacao_post DESC
                          LIMIT 1";
                }

                $result = $PDO->query($sql);
                $rows = $result->fetchAll();
                $count = $result->rowCount();

                foreach($rows as $post) {
                  if($post['imagem_post'] != ""){
                    echo "
                        <div class='container-img-main-box'>
                          ".$post['imagem_post']."
                        </div>
                        ";
                  }
                  echo "
                      <div class='container-info-main-box'>
                        <h5>".$post['titulo_post']."</h5>
                        <span class='box-news-post-date'><i class='fa fa-calendar left mr5' aria-hidden='true'></i><span class='left'></span>".date("d/m/Y", strtotime($post['data_criacao_post']))."</span>
                        <span class='box-news-post-desc'>
                          ".$post['texto_post']."
                        </span>
                      </div>
                  ";
                }
              ?>
            </article>

            <aside class="container-box-news">
            <!--<h6 class="box-txt-noticias-anteriores-mobile">Notícias anteriores</h6>-->
              <!-- Box 1 -->
              <article class="box-news box-2 box2-<?php echo rand(1, 6); ?>">
                <?php
                  if($operacao != "" && $count != "0"){
                    $sql = "SELECT * FROM tb_posts
                            WHERE id_operacoes LIKE '%$id_operacao,%'
                            AND post_status = '1'
                            AND box_post = '2'
                            ORDER BY data_criacao_post DESC
                            LIMIT 1";
                  } else {
                    $sql = "SELECT * FROM tb_posts
                            WHERE id_operacoes LIKE '%$id_operacao,%'
                            AND post_status = '1'
                            AND box_post = '2'
                            ORDER BY data_criacao_post DESC
                            LIMIT 1";
                  }

                  $result = $PDO->query($sql);
                  $rows = $result->fetchAll();
                  $count = $result->rowCount();

                  foreach($rows as $post) {
                    echo "
                        <h6>".$post['titulo_post']."</h6>
                        <span class='box-news-post-date'><i class='fa fa-calendar left mr5' aria-hidden='true'></i><span class='left'>".date("d/m/Y", strtotime($post['data_criacao_post']))."</span></span>
                        <span class='box-news-post-desc'>
                          ".$post['texto_post']."
                        </span>
                    ";
                  }
                ?>
              </article>

              <!-- Box 2 -->
              <article class="box-news box-3 box3-<?php echo rand(1, 6); ?>">
                <?php
                  if($operacao != "" && $count != "0"){
                    $sql = "SELECT * FROM tb_posts
                            WHERE id_operacoes LIKE '%$id_operacao,%'
                            AND post_status = '1'
                            AND box_post = '3'
                            ORDER BY data_criacao_post DESC
                            LIMIT 1";
                  } else {
                    $sql = "SELECT * FROM tb_posts
                            WHERE id_operacoes LIKE '%$id_operacao,%'
                            AND post_status = '1'
                            AND box_post = '3'
                            ORDER BY data_criacao_post DESC
                            LIMIT 1";
                  }

                  $result = $PDO->query($sql);
                  $rows = $result->fetchAll();
                  $count = $result->rowCount();

                  foreach($rows as $post) {
                    echo "
                        <h6>".$post['titulo_post']."</h6>
                        <span class='box-news-post-date'><i class='fa fa-calendar left mr5' aria-hidden='true'></i><span class='left'>".date("d/m/Y", strtotime($post['data_criacao_post']))."</span></span>
                        <span class='box-news-post-desc'>
                          ".$post['texto_post']."
                        </span>
                    ";
                  }
                ?>
              </article>

              <!-- Box 3 -->
              <article id="orbitall" class="box-news box-4 box4-<?php echo rand(1, 6); ?>">
                <?php
                  if($operacao != "" && $count != "0"){
                    $sql = "SELECT * FROM tb_posts
                            WHERE id_operacoes LIKE '%$id_operacao,%'
                            AND post_status = '1'
                            AND box_post = '4'
                            ORDER BY data_criacao_post DESC
                            LIMIT 1";
                  } else {
                    $sql = "SELECT * FROM tb_posts
                            WHERE id_operacoes LIKE '%$id_operacao,%'
                            AND post_status = '1'
                            AND box_post = '4'
                            ORDER BY data_criacao_post DESC
                            LIMIT 1";
                  }

                  $result = $PDO->query($sql);
                  $rows = $result->fetchAll();
                  $count = $result->rowCount();

                  foreach($rows as $post) {
                    echo "
                        <h6>".$post['titulo_post']."</h6>
                        <span class='box-news-post-date'><i class='fa fa-calendar left mr5' aria-hidden='true'></i><span class='left'>".date("d/m/Y", strtotime($post['data_criacao_post']))."</span></span>
                        <span class='box-news-post-desc'>
                          ".$post['texto_post']."
                        </span>
                    ";
                  }
                ?>
              </article>
            </aside>
          </div>
        </div>
      </section>

      <!-- Footer -->
      <?php include "partials/footer.php"; ?>
    </div>
    <!-- Site Container -->

    <script>
      $(document).ready(function(){
        $(".container-img-main-box p img").removeAttr("style height width border alt valign hspace vspace");
      });
    </script>

  </body>
</html>
