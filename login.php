<?php
  session_start();
  session_destroy();
  include "partials/head.php";
?>

  <body class="login carregando-<?php echo rand(1, 6); ?>">
    <!-- Loader -->
    <?php include "partials/loader.php"; ?>

    <!-- Site Container -->
    <div class="site">
      <!-- Loader -->

      <!-- Home -->
      <section class="container-content">
        <div class="container">

          <!-- Main content -->
          <div class="main-content">
            <div class="container-login">
              <div class="row">
                <img src="src/img/logo-orbitall-branco.png" class="logo-login" />
              </div>

              <form class="aba-login" id="form-login">
                <input type='hidden' name='acao' value='login' />
                <div class="row">
                  <span class="label-login">Login</span>
                  <input type="text" class="input-login login-user" name="login-user" />
                </div>
                <div class="row mt20">
                  <span class="label-login">Senha</span>
                  <input type="password" class="input-login login-pass left wd80per" name="login-pass" />
                  <div class='right wd10per'>
                    <button type='button' class='btn-ver-senha-login'><i class="fa fa-eye-slash olho-senha-selec" aria-hidden="true"></i><i class="fa fa-eye olho-senha" aria-hidden="true"></i></button>
                  </div>
                </div>

                <div class="row mt40">
                  <button type="button" class="btn-login">ENTRAR</button>
                </div>
              </form>

              <div class="aba-login container-mensagem-login"></div>

            </div>
          </div>
        </div>
      </section>

      <!-- Footer -->
      <?php include "partials/footer.php"; ?>
    </div>
    <!-- Site Container -->

    <script>
      $(document).keypress(function(e) {
          if(e.which == 13) {
            $(".btn-login").click();
          }
      });
    </script>
    
  </body>
</html>
