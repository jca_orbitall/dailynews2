/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
  config.allowedContent = true;
  config.removeButtons = 'Underline,Find,Replace,SelectAll,Scayt,Form,Checkbox,ImageButton,Radio,TextField,Textarea,Select,Button,HiddenField,Bold,Italic,Underline,Strike,Subscript,Superscript,RemoveFormat,JustifyCenter,Source,CopyFormating,CopyFormatting,CopyFormat,Formating,Image,Save,NewPage,DocProps,Preview,Print,Templates,Document,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,NumberedList,BulletedList,Outdent,Indent,Blockquote,CreateDiv,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,BidiLtr,BidiRtl,Link,Unlink,Anchor,CreatePlaceholder,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,InsertPre,Styles,Format,Font,FontSize,TextColor,BGColor,UIColor,Maximize,ShowBlocks,button1,button2,button3,oembed,MediaEmbed,About';
};
