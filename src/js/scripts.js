$(function(){

	$(".carregando").show(); // Loader

	//Trigger de funcoes...Incluir gestor, usuario e operacao
	$("body").on("click", ".btn-add", function(){
		var incluir = $(this).attr("rel");

		switch(incluir) {
			case "post": {
				$(".mask-modal-inclui-post").fadeIn();
				$("body").addClass("overflow");
			};
			break;

			case "gestor": {
				$(".mask-modal-inclui-gestor").fadeIn();
				$("body").addClass("overflow");
			};
			break;

			case "operacao": {
				$(".mask-modal-inclui-operacao").fadeIn();
				$("body").addClass("overflow");
			};
			break;

			case "usuario": {
				$(".mask-modal-inclui-usuario").fadeIn();
				$("body").addClass("overflow");
			};
			break;
		}
	});


	//login
	$(document).on("click", ".btn-ver-senha-login, .btn-ver-senha-login-txt", function(){
		if($(".login-pass").attr('type') != "password") {
			$(".login-pass").attr('type', 'password');
			$(".olho-senha-selec").hide();
			$(".olho-senha").show();
		} else {
			$(".login-pass").attr('type', 'text');
			$(".olho-senha-selec").show();
			$(".olho-senha").hide();
		}
	});

	$(".btn-login").click(function(){
		var dadosLogin = $("#form-login").serialize(),
				valid = "no";

		if($(".login-user").val() == ""){
			$(".login-user").addClass("borda-vermelha").attr("placeholder", "Confira seu usuário");
			$(".container-mensagem-login").html("").append("<br><span class=''>Provavelmente seu usuário está errado...</span>").fadeIn();
			valid = "no";
		} else {
			$(".login-user").removeClass("borda-vermelha").attr("placeholder", "Digite seu usuário");
			$(".container-mensagem-login").html("");
			valid = "yes";
		}

		if($(".login-pass").val() == ""){
			$(".login-pass").addClass("borda-vermelha").attr("placeholder", "Confira sua senha");
			$(".container-mensagem-login").html("").append("<br><span class=''>Tem certeza que a senha está certa? <span class='btn-ver-senha-login-txt'>Clique aqui para vê-la.</span>").fadeIn();
			valid = "no";
		} else {
			$(".login-pass").removeClass("borda-vermelha").attr("placeholder", "E agora, a sua senha");
			valid = "yes";
		}

		if(valid == "yes"){
			$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'functions.php',
					async: true,
					data: dadosLogin,
					beforeSend: function(data) {
						$(".container-mensagem-login").html("").append("<br><span class=''>Efetuando discagem 1... Velocidade: 14.400 bps...</span>").fadeIn();
					},
					success: function(data) {
						if(data != '0'){
							setTimeout(function(){
								$(".container-mensagem-login").html("").append("<br><span class=''>Conectando... <br>Por favor, aguarde enquanto preparamos um café...</span>").fadeIn();
							}, 1000);

							setTimeout(function(){
								window.location.href = "admin";
							}, 3000);
						} else {
							setTimeout(function(){
								$(".container-mensagem-login").html("").append("<br><span class=''>Algo deu errado com seu login e senha... <br>Por favor, tente novamente.</span>").fadeIn();
							}, 200);
						}
					},
					error: function(data) {
						$(".container-mensagem-login").html("").append("<span class=''>Infelizmente aconteceu um erro de sistema. Por favor, tente novamente. <br>Continuando o erro, por favor, <a href='mailto:sssantos@stefanini.com'>clique aqui</a>.</span>");
					},
					complete: function(data) {
						console.log(data)
					}
			});
		}
	});


	//Menu principal
	$(".btn-menu-admin").on("click", function(){
		var menuAcao = $(this).attr('rel');
		if(menuAcao == "sair"){
				var htmlModalSair = "<div class='content-modal'>"
							+"<div class='line'>"
								+"<div class='left'>"
									+"<h6 class='excluir-msg-certeza'>Já? Tem certeza que deseja sair?</h6>"
								+"</div>"
							+"</div>"
							+"<div class='row mt20'>"
								+"<button type='button' class='btn btn-error left mr20' id='btnSair' rel='"+$(this).closest('#form-edita-post').attr('rel')+"'><i class='fa fa-exclamation-circle' aria-hidden='true'></i>DESEJO SAIR</button>"
								+"<button type='button' class='btn btn-edit right btn-close-modal-sair'><i class='fa fa-arrow-left' aria-hidden='true'></i>VOU FECHAR E VOLTAR</button>"
							+"</div>"
					+"</div>";
					$(".mask-modal-sair .mask-modal-sair-content").html("");
					$(".mask-modal-sair .mask-modal-sair-content").append(htmlModalSair);
					$(".mask-modal-sair").fadeIn();
		} else {
			$('.main-admin-content').hide();
			$('.container-'+ menuAcao +'').show();
			$(".btn-menu-admin").removeClass('menu-admin-active');
			$(this).addClass('menu-admin-active');
		}
	});

	$(".btn-menu-admin[rel=posts]").addClass('menu-admin-active');
	$('.container-posts').show();


	$(document).on("click", "#btnSair", function(){
		window.location = 'logout';
	});

	$(document).on("click", ".btn-close-modal-sair", function(){
		$(".mask-modal-sair").hide();
	});



/*
	valida-input-titulo-post
  valida-tags-operacoes
	valida-textarea-texto-post

	sao campos ocultos para validacao do formulario para enviar post
	*/

//Remove click e da opacidade aos botoes de publicar post
$(".ct-btn-novo-post-passo-2 .mask-block").show();
$(".ct-btn-publicar-post .mask-block").show();

$(document).on("keyup", "#form-inclui-post .input-titulo-post", function(){
	if($(this).val() != "") {
		$(this).removeClass("borda-vermelha");
		$(".valida-input-titulo-post").val('1');

		if($("#form-inclui-post .textarea-texto-post").val() == ""){
			$("#form-inclui-post .textarea-texto-post").addClass("borda-vermelha");
			$(".valida-textarea-texto-post").val('0');
		} else {
			$("#form-inclui-post .textarea-texto-post").removeClass("borda-vermelha");
			$(".valida-textarea-texto-post").val('1');
		}

		if($("#form-inclui-post .tag-operacao:visible").size() < 1){
			$(".select-operacoes-inclui-post .trigger-simple-select").addClass("borda-vermelha");
			$(".valida-tags-operacoes").val('0');
		} else {
			$(".select-operacoes-inclui-post .trigger-simple-select").removeClass("borda-vermelha");
			$(".valida-tags-operacoes").val('1');
		}

		if($(".valida-input-titulo-post").val() == "1" && $(".valida-tags-operacoes").val() == "1" && $(".valida-textarea-texto-post").val() == "1") {
			$("#form-inclui-post .ct-btn-novo-post-passo-2 .mask-block").hide();
		}

	} else {
		$(this).addClass("borda-vermelha");
		$(".valida-input-titulo-post").val('0');
		$("#form-inclui-post .ct-btn-novo-post-passo-2 .mask-block").show();
	}
});

//Select para operacoes em novo post.
$("body").on("click", ".select-operacoes-inclui-post ul.simple-select li", function(){
	if($("#form-inclui-post .textarea-texto-post").val() == ""){
		$("#form-inclui-post .textarea-texto-post").addClass("borda-vermelha");
		$(".valida-textarea-texto-post").val('0');
	} else {
		$("#form-inclui-post .textarea-texto-post").removeClass("borda-vermelha");
		$(".valida-textarea-texto-post").val('1');
	}

	if($("#form-inclui-post .input-titulo-post").val() == ""){
		$("#form-inclui-post .input-titulo-post").addClass("borda-vermelha");
		$(".valida-input-titulo-post").val('0');
	} else {
		$("#form-inclui-post .input-titulo-post").removeClass("borda-vermelha");
		$(".valida-input-titulo-post").val('1');
	}

	if($(".select-operacoes-inclui-post .trigger-simple-select").hasClass("borda-vermelha")) {
		$(".select-operacoes-inclui-post .trigger-simple-select").removeClass("borda-vermelha");
		$(".valida-tags-operacoes").val('1');
	}

	if(!($(this).hasClass('sel-tags-oper-active'))) {
		$("#form-inclui-post .operacoes-selecionadas-post").fadeIn();
		$(this).addClass('sel-tags-oper-active');
		$("#form-inclui-post .operacoes-selecionadas-post").append("<div tag-operacao='"+$(this).attr("id")+"' class='tag-operacao tag-"+$(this).attr("rel")+"'><span>"+$(this).attr("rel")+"</span><i class='fa fa-times-circle' aria-hidden='true'></i></div>");
		checkTags();
	} else {
		$(".tag-operacao").removeClass('animated bounce zoomOut');
		$(".tag-"+$(this).attr("rel")+"").addClass('animated bounce');
		setTimeout(function(){
			$(".tag-operacao").removeClass('animated bounce zoomOut');
		}, 5000);
		checkTags();
	}

	if($(".valida-input-titulo-post").val() == "1" && $(".valida-tags-operacoes").val() == "1" && $(".valida-textarea-texto-post").val() == "1") {
		$("#form-inclui-post .ct-btn-novo-post-passo-2 .mask-block").hide();
	} else {
		$("#form-inclui-post .ct-btn-novo-post-passo-2 .mask-block").show();
	}

});

//Textarea
$(document).on("keyup", "#form-inclui-post .textarea-texto-post", function(){
	if($(this).val() != "") {
		$(this).removeClass("borda-vermelha");
		$(".valida-textarea-texto-post").val('1');

		if($("#form-inclui-post .input-titulo-post").val() == ""){
			$("#form-inclui-post .input-titulo-post").addClass("borda-vermelha");
			$(".valida-input-titulo-post").val('0');
		} else {
			$("#form-inclui-post .input-titulo-post").removeClass("borda-vermelha");
			$(".valida-input-titulo-post").val('1');
		}

		if($("#form-inclui-post .tag-operacao:visible").size() < 1){
			$(".select-operacoes-inclui-post .trigger-simple-select").addClass("borda-vermelha");
			$(".valida-tags-operacoes").val('0');
		} else {
			$(".select-operacoes-inclui-post .trigger-simple-select").removeClass("borda-vermelha");
			$(".valida-tags-operacoes").val('1');
		}

		if($(".valida-input-titulo-post").val() == "1" && $(".valida-tags-operacoes").val() == "1" && $(".valida-textarea-texto-post").val() == "1") {
			$("#form-inclui-post .ct-btn-novo-post-passo-2 .mask-block").hide();
		}

	} else {
		$(this).addClass("borda-vermelha");
		$("#form-inclui-post .ct-btn-novo-post-passo-2 .mask-block").show();
		$(".valida-textarea-texto-post").val('0');
	}
});

//Next-Prev para os passos do novo post
$(document).on("click", "#form-inclui-post .btn-steps-modal", function(){
	$('#form-inclui-post .aba-formulario').hide();
	$('#form-inclui-post .'+ $(this).attr('rel') +'').show();
	$("#form-inclui-post .box-pos-check").hide();
	$(this).find(".box-pos-check").fadeIn();
});


	//Publicar Post
	$(document).on("click", "#form-inclui-post #btn-publicar-post", function(){
		$('.aba-formulario').hide();
		$('.result-inclui-post').show();
/*
		var string = $(".novo-post-operacoes").val();

		var operacoes=string.split(',').filter(function(item,i,allItems){
		    return i==allItems.indexOf(item);
		}).join(',');
*/
		var editor = CKEDITOR.instances['post_editor'].getData();
		CKEDITOR.instances['post_editor'].updateElement();

		$("#form-inclui-post").append("<input type='hidden' name='acao' value='incluiPost' />");

		var dadosIncluiPost = $(this).closest("#form-inclui-post").serialize();

		$.ajax({
				type: 'POST',
				dataType: 'json',
				url: 'functions.php',
				async: true,
				data: dadosIncluiPost,
				beforeSend: function(data) {
					for (instance in CKEDITOR.instances) {
							CKEDITOR.instances[instance].updateElement();
					}
					$(".result-inclui-post .mensagem-result-inclui-post").hide();
					$(".result-inclui-post .mensagem-result-inclui-post-enviando").show();
				},
				complete: function(data) {
					setTimeout(function() {
						$(".result-inclui-post .mensagem-result-inclui-post").hide();
						$(".result-inclui-post .mensagem-result-inclui-post-sucesso").show();
					},2000);
					setTimeout(function() {
						$(".mask-modal-inclui-post").hide();
						window.location.reload();
					},5000);
				}
		});
	});

	//add limpa modal ao botao fechar da mesma modal para novo post
	$("body").on("click", ".mask-modal-inclui-post .modal-title .btn-close-modal", function(){
		limpaModalIncluiPost();
	});

	//Limpa modal novo post
	function limpaModalIncluiPost() {
		$(".mask-modal-inclui-post").fadeOut();
		$(".aba-formulario").hide();
		$("#form-inclui-post .novo-post-passo-1").show();

		$(".chars-count-red").text("210");
		$(".valida-input-titulo-post, .valida-tags-operacoes, .valida-textarea-texto-post").val("0");
		$("#form-inclui-post .ct-btn-novo-post-passo-2 .mask-block").show();
		$("#form-inclui-post .ct-btn-publicar-post .mask-block").show();

		$("#form-inclui-post input[type=text]").val("");
		$("#form-inclui-post .post-status[rel=ativo]").removeAttr("checked");
		$("#form-inclui-post .post-status[rel=inativo]").prop("checked", true);
		$("#form-inclui-post textarea").val("").text("").html("");
		$("#form-inclui-post .box-pos").removeClass("box-pos-ativo");

		$(".select-operacoes-inclui-post .simple-select li").removeClass("selected sel-tags-oper-active");
		$(".select-operacoes-inclui-post .trigger-simple-select").removeClass("select-opened trigger-simple-select-active").html("Selecione...").append('<i class="fa fa-caret-up" aria-hidden="true"></i>');

		$("#form-inclui-post .operacoes-selecionadas-post").html("").hide();
		$(".tag-operacao").each(function(){
			$(this).hide().removeClass('animated bounce zoomOut');
		});
	}

	$(".mask-modal-inclui-post").on('click',function(e){
	    if(e.target == this) {
				limpaModalIncluiPost();
			}
	});


//Edita Post
$("body").on("click", ".btn-trigger-title-post-table", function() {
	var id_post = $(this).attr('id');
	$(".mask-modal-edita-post").fadeIn();
	$("body").addClass("overflow");
	$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'functions.php',
			async: true,
			data: {id_post: id_post, acao: "editaPost"},
			complete: function(data) {
				$(".mask-modal-edita-post").html("<div class='conteiner-modal'>"+data.responseText+"</div>");
				//Remove ultima tag vazia
				$(".tag-operacao").each(function(){
					if($(this).attr("tag-operacao") == "" || $(this).attr("tag-operacao") == "-1"){
						$(this).detach();
					}
				});

				$("#form-edita-post .aba-formulario .ct-btn-novo-post-passo-2 .mask-block").hide();

				CKEDITOR.replace( 'post_editor2', {
					extraPlugins : 'base64image',
					height:['0px']
				});

				$(".box-img-edit-post p img").removeAttr("style height width border alt valign hspace vspace");

				$("#form-edita-post .box-pos.box-pos[rel="+$("#form-edita-post .inp-hidden-box-pos").val()+"]").addClass("box-pos-ativo");
				$("#form-edita-post .box-pos.box-pos[rel="+$("#form-edita-post .inp-hidden-box-pos").val()+"] .box-pos-check").show();

				if($("#form-edita-post .post-status").val() == "1") {
					$("#form-edita-post .post-status[rel=ativo]").prop('checked', true);
					$("#form-edita-post .post-status[rel=inativo]").prop('checked', false);
					$("#form-edita-post .post-status").val("1");
				} else {
					$("#form-edita-post .post-status[rel=ativo]").prop('checked', false);
					$("#form-edita-post .post-status[rel=inativo]").prop('checked', true);
					$("#form-edita-post .post-status").val("2");
				}
			}
	});
});


//Next-Prev para os passos do novo post
$(document).on("click", "#form-edita-post .btn-steps-modal", function(){
	$('#form-edita-post .aba-formulario').hide();
	$('#form-edita-post .'+ $(this).attr('rel') +'').show();
	//$("#form-edita-post .box-pos-check").hide();
	//$(this).find(".box-pos-check").fadeIn();
});


$(document).on("keyup", "#form-edita-post .input-titulo-post", function(){
	if($(this).val() != "") {
		$(this).removeClass("borda-vermelha");
		$("#form-edita-post .valida-edita-input-titulo-post").val('1');

		if($("#form-edita-post .textarea-texto-post").val() == ""){
			$("#form-edita-post .textarea-texto-post").addClass("borda-vermelha");
			$("#form-edita-post .valida-edita-textarea-texto-post").val('0');
		} else {
			$("#form-edita-post .textarea-texto-post").removeClass("borda-vermelha");
			$("#form-edita-post .valida-edita-textarea-texto-post").val('1');
		}

		if($("#form-edita-post .tag-operacao:visible").size() < 1){
			$(".select-operacoes-edita-post .trigger-simple-select").addClass("borda-vermelha");
			$("#form-edita-post .valida-edita-tags-operacoes").val('0');
		} else {
			$(".select-operacoes-inclui-post .trigger-simple-select").removeClass("borda-vermelha");
			$("#form-edita-post .valida-edita-tags-operacoes").val('1');
		}

		if($("#form-edita-post .valida-edita-input-titulo-post").val() == "1" && $("#form-edita-post .valida-edita-tags-operacoes").val() == "1" && $("#form-edita-post .valida-edita-textarea-texto-post").val() == "1") {
			$("#form-edita-post .ct-btn-novo-post-passo-2 .mask-block").hide();
		}

	} else {
		$(this).addClass("borda-vermelha");
		$("#form-edita-post .valida-edita-input-titulo-post").val('0');
		$("#form-edita-post .ct-btn-novo-post-passo-2 .mask-block").show();
	}
});



//Edita post//Select para operacoes em novo post.
$("body").on("click", ".select-operacoes-edita-post ul.simple-select li", function(){
	var thisID = $(this).attr('id');
	if($("#form-edita-post .textarea-texto-post").val() == ""){
		$("#form-edita-post .textarea-texto-post").addClass("borda-vermelha");
		$("#form-edita-post .valida-edita-textarea-texto-post").val('0');
	} else {
		$("#form-edita-post .textarea-texto-post").removeClass("borda-vermelha");
		$("#form-edita-post .valida-edita-textarea-texto-post").val('1');
	}

	if($("#form-edita-post .input-titulo-post").val() == ""){
		$("#form-edita-post .input-titulo-post").addClass("borda-vermelha");
		$("#form-edita-post .valida-edita-input-titulo-post").val('0');
	} else {
		$("#form-edita-post .input-titulo-post").removeClass("borda-vermelha");
		$("#form-edita-post .valida-edita-input-titulo-post").val('1');
	}

	if($(".select-operacoes-edita-post .trigger-simple-select").hasClass("borda-vermelha")) {
		$(".select-operacoes-edita-post .trigger-simple-select").removeClass("borda-vermelha");
		$("#form-edita-post .valida-edita-tags-operacoes").val('1');
	}

	var thisForm = $(this).closest("#form-edita-post"),
			thisID = $(this).attr('id'),
			thisRel = $(this).attr('rel'),
			thisTag = thisForm.find(".tag-operacao[tag-operacao='"+thisID+"']");

	if(thisTag.is(":visible")) {
		$(".tag-operacao").removeClass('animated bounce zoomOut');
		thisTag.addClass('animated bounce');
		checkTags();
	} else {
		thisForm.find(".operacoes-selecionadas-post").append("<div tag-operacao='"+thisID+"' class='tag-operacao tag-"+thisRel+"'><span>"+thisRel+"</span><i class='fa fa-times-circle' aria-hidden='true'></i></div>");
		checkTags();
	}

/*
	if(!($(this).hasClass('sel-tags-oper-active'))) {
		$("#form-edita-post .operacoes-selecionadas-post").fadeIn();
		$(this).addClass('sel-tags-oper-active');

		if($("#form-edita-post .tag-operacao:visible").attr("tag-operacao") == $(this).attr("id")) {
			$(".tag-operacao").removeClass('animated bounce zoomOut');
			$(".tag-"+$(this).attr("rel")+"").addClass('animated bounce');
			setTimeout(function(){
				$(".tag-operacao").removeClass('animated bounce zoomOut');
			}, 5000);
		} else {
			$("#form-edita-post .operacoes-selecionadas-post").append("<div tag-operacao='"+$(this).attr("id")+"' class='tag-operacao tag-"+$(this).attr("rel")+"'><span>"+$(this).attr("rel")+"</span><i class='fa fa-times-circle' aria-hidden='true'></i></div>");
		}
		checkTags();
	} else {
		$("#form-edita-post .tag-operacao").removeClass('animated bounce zoomOut');
		$(".tag-"+$(this).attr("rel")+"").addClass('animated bounce');
		setTimeout(function(){
			$(".tag-operacao").removeClass('animated bounce zoomOut');
		}, 5000);
		checkTags();
	}
	*/

	if($("#form-edita-post .valida-edita-input-titulo-post").val() == "1" && $("#form-edita-post .valida-edita-tags-operacoes").val() == "1" && $("#form-edita-post .valida-edita-textarea-texto-post").val() == "1") {
		$("#form-edita-post .ct-btn-novo-post-passo-2 .mask-block").hide();
	} else {
		$("#form-edita-post .ct-btn-novo-post-passo-2 .mask-block").show();
	}
});

//Textarea
$(document).on("keyup", "#form-edita-post .textarea-texto-post", function(){
	if($(this).val() != "") {
		$(this).removeClass("borda-vermelha");
		$(".valida-edita-textarea-texto-post").val('1');

		if($("#form-edita-post .input-titulo-post").val() == ""){
			alert($("#form-edita-post .input-titulo-post").val());
			$("#form-edita-post .input-titulo-post").addClass("borda-vermelha");
			$(".valida-edita-input-titulo-post").val('0');
		} else {
			$("#form-edita-post .input-titulo-post").removeClass("borda-vermelha");
			$(".valida-edita-input-titulo-post").val('1');
		}

		if($("#form-edita-post .tag-operacao:visible").size() < 1){
			$(".select-operacoes-inclui-post .trigger-simple-select").addClass("borda-vermelha");
			$(".valida-edita-tags-operacoes").val('0');
		} else {
			$(".select-operacoes-inclui-post .trigger-simple-select").removeClass("borda-vermelha");
			$(".valida-edita-tags-operacoes").val('1');
		}

		if($(".valida-edita-input-titulo-post").val() == "1" && $(".valida-edita-tags-operacoes").val() == "1" && $(".valida-edita-textarea-texto-post").val() == "1") {
			$("#form-edita-post .ct-btn-novo-post-passo-2 .mask-block").hide();
		}

	} else {
		$(this).addClass("borda-vermelha");
		$("#form-edita-post .ct-btn-novo-post-passo-2 .mask-block").show();
		$(".valida-edita-textarea-texto-post").val('0');
	}
});


//BTN Exclui img do post
$(document).on("click", ".btn-exclui-img-post", function(){
	$("#form-edita-post .aba-formulario").hide();
	$("#form-edita-post .ct-modal-exclui-imagem-post").show();

	var htmlModal = "<div class='content-modal'>"
				+"<div class='line'>"
					+"<div class='left'>"
						+"<h6 class='excluir-msg-certeza'>Tem certeza que deseja excluir?</h6>"
					+"</div>"
				+"</div>"
				+"<div class='row mt20'>"
					+"<button type='button' class='btn btn-error left mr20' id='excluirImagemPostDefinitivo' rel='"+$(this).closest('#form-edita-post').attr('rel')+"'><i class='fa fa-exclamation-circle' aria-hidden='true'></i>DESEJO EXCLUIR</button>"
					+"<button type='button' class='btn btn-edit right btn-close-modal-exclui-imagem-post'><i class='fa fa-arrow-left' aria-hidden='true'></i>VOU FECHAR E VOLTAR</button>"
				+"</div>"
		+"</div>";

	$("#form-edita-post .ct-modal-exclui-imagem-post").html("");
	$("#form-edita-post .ct-modal-exclui-imagem-post").append(htmlModal);
});

$(document).on("click", ".btn-close-modal-exclui-imagem-post", function(){
	$("#form-edita-post .ct-modal-exclui-imagem-post").hide();
	$("#form-edita-post .novo-post-passo-2").show();
});

$(document).on("click", "#excluirImagemPostDefinitivo", function(){
	$(this).closest("#form-edita-post").find(".box-img-edit-post p").html("");
	$(this).closest("#form-edita-post").find(".hidden_post_editor").val("");
	$(this).closest("#form-edita-post").find(".box-img-edit-post").remove("p");
	$(this).closest("#form-edita-post").find(".box-img-edit-post").hide();
	$(this).closest("#form-edita-post").find("#post_editor").val("");
	$(this).closest("#form-edita-post").find("#post_editor2").val("");

	$("#form-edita-post .ct-modal-exclui-imagem-post").hide();
	$("#form-edita-post .novo-post-passo-2").show();

	CKEDITOR.instances['post_editor2'].on('change', function() {
		$(".box-img-edit-post").html("");
		CKEDITOR.instances['post_editor2'].updateElement();
		$("#form-edita-post .box-img-edit-post").html(CKEDITOR.instances.post_editor2.getData()+"<button type='button' alt='Excluir essa imagem' title='Excluir essa imagem' class='btn-exclui-img-post'><i class='fa fa-close' aria-hidden='true'></i></button>");
		$("#form-edita-post .box-img-edit-post").show();
		$("#form-edita-post .box-img-edit-post p img").removeAttr("style height width border alt valign hspace vspace");
	});
});


$(document).on("click", "#form-edita-post #btn-editar-post", function(){
	$('.aba-formulario').hide();
	$('.result-edita-post').show();
/*
	var string = $(".novo-post-operacoes").val();

	var operacoes=string.split(',').filter(function(item,i,allItems){
			return i==allItems.indexOf(item);
	}).join(',');
*/
	var editor = CKEDITOR.instances['post_editor2'].getData();
	CKEDITOR.instances['post_editor2'].updateElement();

	$("#form-edita-post").append("<input type='hidden' name='acao' value='editaPublicaPost' />");

	var dadosEditaPost = $(this).closest("#form-edita-post").serialize();

	console.log(dadosEditaPost);

	$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'functions.php',
			async: true,
			data: dadosEditaPost,
			beforeSend: function(data) {
				for (instance in CKEDITOR.instances) {
						CKEDITOR.instances[instance].updateElement();
				}
				$(".result-edita-post .mensagem-result-edita-post").hide();
				$(".result-edita-post .mensagem-result-edita-post-enviando").show();
			},
			complete: function(data) {
				setTimeout(function() {
					$(".result-edita-post .mensagem-result-edita-post").hide();
					$(".result-edita-post .mensagem-result-edita-post-sucesso").show();
				},2000);
				setTimeout(function() {
					$(".mask-modal-edita-post").hide();
					window.location.reload();
				},5000);
			}
		});
	});



	//Botao para selecionar qual box receberá a notícia
	$(document).on("click", ".box-pos", function(){
		if(!($(this).hasClass("box-pos-ativo"))) {
			$(".box-pos").removeClass("box-pos-ativo");
			$(this).addClass("box-pos-ativo");
			$(".box-pos-check").hide();
			$(this).find(".box-pos-check").fadeIn();
			$(".inp-hidden-box-pos").val($(this).attr("rel"));
			$(".ct-btn-publicar-post .mask-block").hide();
		} else {
			$(".box-pos").removeClass("box-pos-ativo");
			$(".inp-hidden-box-pos").val("");
			$(".box-pos-check").hide();
			$(".ct-btn-publicar-post .mask-block").show();
		}
	});


	//Select
	$(".trigger-simple-select").each(function(){
		$(this).html("Selecione...");
		$(this).append('<i class="fa fa-caret-down" aria-hidden="true"></i>');
	});

	$("body").on("click", ".trigger-simple-select", function(){
		if($(this).hasClass("select-opened")){
			$(this).parent().find(".simple-select").slideUp("fast");
			$(this).html("Selecione...").append('<i class="fa fa-caret-down" aria-hidden="true"></i>').removeClass("select-opened");
		} else {
			$(this).parent().find(".simple-select").slideDown("fast");
			$(this).html("Selecione...").append('<i class="fa fa-caret-up" aria-hidden="true"></i>').addClass("select-opened");
		}

		if($(this).html() != "Selecione...") {
			$(this).addClass("trigger-simple-select-active");
		} else {
			$(this).removeClass("trigger-simple-select-active");
		}
	});

	$("body").on("click", ".simple-select li", function(){
		$(this).parent().find("li").removeClass("selected");
		$(this).addClass("selected");
		$(this).parent().parent().parent().find(".trigger-simple-select").text($(this).text()).append('<i class="fa fa-caret-down" aria-hidden="true"></i>').removeClass("select-opened");
		$(this).parent().slideUp("fast");
		$(this).parent().parent().parent().find(".hidden-operacao").val($(this).attr('id'));
	});


	//Modal
	$(document).on("click", ".btn-close-modal, .btn-close-modal-big", function(){
		$(this).closest(".mask-modal").fadeOut();
		$("body").removeClass("overflow");
	});



	//Evento que pega a tecla Esc pressionada
	$(document).keyup(function(e) {
	     if (e.keyCode == 27) {
				 if($(".mask-modal").is(":visible")){
					 $(".mask-modal").fadeOut();
					 $("body").removeClass("overflow");
				 }
	    }
	});


	//Clique na mascara para fechar
	$(".mask-modal").on('click',function(e){
	    if(e.target == this) {
				$(this).fadeOut();
				$("body").removeClass("overflow");
			}
	});

	$(".trigger-remove-mask-block").click(function() {
		$(".select-blocked").toggle();
	});


	//Confere se existem tags para as operacoes e montam o valor do campo oculto que vai ao BD
	function checkTags() {
		var tagsCount = $(".tag-operacao:visible").size();

		var arr = [];
		for(var i = 0; i < tagsCount; i++) {
			arr[i] = $(".tag-operacao:visible").eq(i).attr("tag-operacao");
		}

		$(".operacoes-selecionadas-post").append("<input type='hidden' name='novo-post-operacoes' class='novo-post-operacoes' />");
		$(".novo-post-operacoes").val(arr.join(',')+",");
		if(tagsCount == 0) {
			$(".novo-post-operacoes").val("");
			$(".valida-tags-operacoes, .valida-edita-tags-operacoes").val('0');
			$(".ct-btn-novo-post-passo-2 .mask-block").show();
		}
	}


	//Clique para as tags (remove a mesma ao clicar)
	$("body").on("click", ".tag-operacao", function(){
		$(this).removeClass('animated bounce zoomOut');
		$(this).addClass('animated zoomOut');
		$(this).removeClass('animated bounce zoomOut').hide();
		$(".select-operacoes-inclui-post ul.simple-select li#"+$(this).attr("tag-operacao")+"").removeClass('sel-tags-oper-active');
		if($(".tag-operacao:visible").size() == 0) {
			$(".operacoes-selecionadas-post").hide();
			$(".select-operacoes-inclui-post .trigger-simple-select").addClass("borda-vermelha").text("Selecione...").append('<i class="fa fa-caret-down" aria-hidden="true"></i>').removeClass("select-opened");
		}
		checkTags();
	});

	//Todas as operacoes  - Select para novo post.
	$(".select-operacoes-inclui-post").on("click", ".todas", function(){
		$(".select-operacoes-inclui-post .todas").parent().find("li").removeClass("selected");
		$(".select-operacoes-inclui-post .todas").addClass("selected");
		$(".select-operacoes-inclui-post .todas").parent().parent().parent().find(".trigger-simple-select").text($(".todas").text()).append('<i class="fa fa-caret-down" aria-hidden="true"></i>').removeClass("select-opened");
		$(".select-operacoes-inclui-post .todas").parent().slideUp("fast");
		$(".select-operacoes-inclui-post .todas").parent().parent().parent().find(".hidden-operacao").val($(this).attr('id'));
	});


	//Insere uma lista com a opcao para selecionar todas as operacoes
	var li_todas_operacoes = "<li class='todas' rel='todas'>Todas as operações</li>";

	$(".select-operacoes-inclui-post").click(function(){
		$(".select-operacoes-inclui-post ul.simple-select .todas").detach();
		$(".select-operacoes-inclui-post ul.simple-select").append(li_todas_operacoes);
	});

	$(".trigger-lista-operacoes-posts").click(function(){
		$(".trigger-lista-operacoes-posts ul.simple-select .todas").detach();
		$(".trigger-lista-operacoes-posts ul.simple-select").append(li_todas_operacoes);
	});

	$(".trigger-lista-gestores-operacao").click(function(){
		$(".trigger-lista-gestores-operacao ul.simple-select .todas").detach();
		$(".trigger-lista-gestores-operacao ul.simple-select").append(li_todas_operacoes);
	});


	//Trigger para a lista de operacoes
	$(".trigger-lista-operacoes-posts li").on("click", function(){
		var id_operacao = $(this).attr("id"),
				dados = id_operacao;
		$.ajax({
				type: 'POST',
				dataType: 'html',
				url: 'functions.php',
				async: true,
				data: {id_operacao: id_operacao, acao: "listaPostsPorOperacao"},
				success: function(data) {
					$(".result-posts").html("").append(data);
					tdNomeOperacao();
				},
				error: function(data) {
					console.log('oh');
				},
				complete: function(data) {
					console.log(data);
				}
		});
	});

	$(".trigger-lista-operacoes-posts").on("click", ".todas", function(){
		$.ajax({
				type: 'POST',
				dataType: 'html',
				url: 'functions.php',
				async: true,
				data: {acao: "listaTodosPosts"},
				success: function(data) {
					$(".result-posts").html("").append(data);
					tdNomeOperacao();
				},
				error: function(data) {
					console.log('oh');
				},
				complete: function(data) {
					$(".todas").parent().find("li").removeClass("selected");
					$(".todas").addClass("selected");
					$(".todas").parent().parent().parent().find(".trigger-simple-select").text($(".todas").text()).append('<i class="fa fa-caret-down" aria-hidden="true"></i>').removeClass("select-opened");
					$(".todas").parent().slideUp("fast");
					$(".todas").parent().parent().parent().find(".hidden-operacao").val($(this).attr('id'));
				}
		});
	});


	//Trigger para a lista de gestores
	$(".trigger-lista-gestores-operacao li").on("click", function(){
		var id_operacao = $(this).attr("rel"),
				dados = id_operacao;
		$.ajax({
				type: 'POST',
				dataType: 'html',
				url: 'functions.php',
				async: true,
				data: {id_operacao: id_operacao, acao: "listaGestoresPorOperacao"},
				success: function(data) {
					$(".result-gestores").html("").append(data);
					tdNomeOperacao();
				},
				error: function(data) {
					console.log('oh');
				},
				complete: function(data) {
					console.log(data);
				}
		});
	});

	$(".trigger-lista-gestores-operacao").on("click", ".todas", function(){
		$.ajax({
				type: 'POST',
				dataType: 'html',
				url: 'functions.php',
				async: true,
				data: {id_operacao: "todas", acao: "listaGestoresPorOperacao"},
				success: function(data) {
					$(".result-gestores").html("").append(data);
					tdNomeOperacao();
				},
				error: function(data) {
					console.log('oh');
				},
				complete: function(data) {
					$(".todas").parent().find("li").removeClass("selected");
					$(".todas").addClass("selected");
					$(".todas").parent().parent().parent().find(".trigger-simple-select").text($(".todas").text()).append('<i class="fa fa-caret-down" aria-hidden="true"></i>').removeClass("select-opened");
					$(".todas").parent().slideUp("fast");
					$(".todas").parent().parent().parent().find(".hidden-operacao").val($(this).attr('id'));
				}
		});
	});



	//Incluir operacao
	$(document).on("click", ".select-tipo-operacao .simple-select li", function(){
		$(".tipo-nova-operacao").val($(this).attr("id"));
		$(".select-tipo-operacao .trigger-simple-select").removeClass("borda-vermelha");
		$(".valida-input-tipo-nova-operacao").val('1');

		if($(".input-nome-nova-operacao").val() == ""){
			$(".input-nome-nova-operacao").addClass("borda-vermelha");
			$(".valida-input-nome-nova-operacao").val('0');
		} else {
			$(".input-nome-nova-operacao").removeClass("borda-vermelha");
			$(".valida-input-nome-nova-operacao").val('1');
		}

		if($(".valida-input-nome-nova-operacao").val() == "1" && $(".valida-input-tipo-nova-operacao").val() == "1") {
			$(".ct-mask-btn-inclui-operacao .mask-block").hide();
		}
	});

	$(document).on("click", "#btn-inclui-operacao", function(){
		var tit = $(".input-nome-nova-operacao").val();
		$("#form-inclui-operacao").append("<input type='hidden' name='acao' value='incluirOperacao' />");
		slug = tit.toLowerCase().replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '').replace(/[áàãâä\{\}\[\]\\\/]/gi, "a").replace(/[éêèë\{\}\[\]\\\/]/gi, "e").replace(/[íìïî\{\}\[\]\\\/]/gi, "i").replace(/[óòôõö\{\}\[\]\\\/]/gi, "o").replace(/[úùûü\{\}\[\]\\\/]/gi, "u").split(" ").join("-").trim();
		$("#form-inclui-operacao").append("<input type='hidden' name='slug-nova-operacao' value='"+slug+"'/>");
		var novaOperacao = $("#form-inclui-operacao").serialize();
		console.log(novaOperacao);
		$.ajax({
				type: 'POST',
				url: 'functions.php',
				dataType: "json",
				async: true,
				data: novaOperacao,
				beforeSend: function(data) {
					$("#form-inclui-operacao").hide();
					$("#result-inclui-operacao").show();
					$(".mensagem-result-inclui-operacao-enviando").show();
				},
				complete: function(data) {
					setTimeout(function() {
						$("#form-inclui-operacao").hide();
						$("#result-inclui-operacao").show();
						$(".mensagem-result-inclui-operacao-enviando").hide();
						$(".mensagem-result-inclui-operacao-sucesso").show();
					},2000);
				}
		});
	});

	//add limpa modal ao botao fechar da mesma modal para novo post
	$("body").on("click", ".mask-modal-inclui-operacao .modal-title .btn-close-modal", function(){
		limpaModalIncluiOperacao();
	});


	//Limpa modal novo post
	function limpaModalIncluiOperacao() {
		$(".valida-input-nome-nova-operacao .valida-input-tipo-nova-operacao").val('0');
		$(".mask-modal-inclui-operacao").fadeOut();
		$(".mask-modal-inclui-operacao .aba-formulario, .mensagem-result-inclui-operacao").hide();
		$("#form-inclui-operacao").show();

		$(".ct-btn-novo-post-passo-2 .mask-block").show();
		$(".ct-btn-publicar-post .mask-block").show();

		$("#form-inclui-operacao input").val("");

		$(".select-tipo-operacao .simple-select li").removeClass("selected sel-tags-oper-active");
		$(".select-tipo-operacao .trigger-simple-select").removeClass("select-opened trigger-simple-select-active").html("Selecione...").append('<i class="fa fa-caret-up" aria-hidden="true"></i>');

		$(".ct-mask-btn-inclui-operacao .mask-block").show();
	}

	$(".mask-modal-inclui-operacao").on('click',function(e){
			if(e.target == this) {
				limpaModalIncluiOperacao();
			}
	});

	$(document).on("keyup", ".input-nome-nova-operacao", function(){
		if($(this).val() != "") {
			$(this).removeClass("borda-vermelha");
			$(".valida-input-nome-nova-operacao").val('1');

			if($(".tipo-nova-operacao").val() == ""){
				$(".select-tipo-operacao .trigger-simple-select").addClass("borda-vermelha");
				$(".valida-input-tipo-nova-operacao").val('0');
			} else {
				$(".select-tipo-operacao .trigger-simple-select").removeClass("borda-vermelha");
				$(".valida-input-tipo-nova-operacao").val('1');
			}

			if($(".valida-input-nome-nova-operacao").val() == "1" && $(".valida-input-tipo-nova-operacao").val() == "1") {
				$(".ct-mask-btn-inclui-operacao .mask-block").hide();
			}

		} else {
			$(this).addClass("borda-vermelha");
			$(".valida-input-titulo-post").val('0');
			$(".ct-mask-btn-inclui-operacao .mask-block").show();
		}
	});



	//Incluir Gestor
	$(document).on("click", ".select-gestor-operacao .simple-select li", function(){
		$(".tipo-nova-operacao").val($(this).attr("id"));
		$(".select-gestor-operacao .trigger-simple-select").removeClass("borda-vermelha");
		$(".valida-select-gestor-operacao").val('1');

		if($(".input-nome-novo-gestor").val() == ""){
			$(".input-nome-novo-gestor").addClass("borda-vermelha");
			$(".valida-input-nome-novo-gestor").val('0');
		} else {
			$(".input-nome-novo-gestor").removeClass("borda-vermelha");
			$(".valida-input-nome-novo-gestor").val('1');
		}

		if($(".valida-input-nome-novo-gestor").val() == "1" && $(".valida-select-gestor-operacao").val() == "1") {
			$(".ct-mask-btn-inclui-gestor .mask-block").hide();
		}
	});

	$(document).on("click", "#btn-inclui-gestor", function(){
		var tit = $(".input-nome-novo-gestor").val();
		$("#form-inclui-gestor").append("<input type='hidden' name='acao' value='incluirGestor' />");
		$("#form-inclui-gestor").append("<input type='hidden' name='operacao-novo-gestor' value='"+$(".select-gestor-operacao .hidden-operacao").val()+"' />");
		var novoGestor = $("#form-inclui-gestor").serialize();
		console.log(novoGestor);
		$.ajax({
				type: 'POST',
				url: 'functions.php',
				dataType: "json",
				async: true,
				data: novoGestor,
				beforeSend: function(data) {
					$("#form-inclui-gestor").hide();
					$("#result-inclui-gestor").show();
					$(".mensagem-result-inclui-gestor-enviando").show();
				},
				complete: function(data) {
					setTimeout(function() {
						$("#form-inclui-gestor").hide();
						$("#result-inclui-gestor").show();
						$(".mensagem-result-inclui-gestor-enviando").hide();
						$(".mensagem-result-inclui-gestor-sucesso").show();
					},2000);
				}
		});
	});

	//add limpa modal ao botao fechar da mesma modal para novo post
	$("body").on("click", ".mask-modal-inclui-gestor .modal-title .btn-close-modal", function(){
		limpaModalIncluiGestor();
	});


	//Limpa modal novo post
	function limpaModalIncluiGestor() {
		$(".valida-input-nome-novo-gestor .valida-select-gestor-operacao").val('0');
		$(".mask-modal-inclui-gestor").fadeOut();
		$(".mask-modal-inclui-gestor .aba-formulario, .mensagem-result-inclui-gestor").hide();
		$("#form-inclui-gestor").show();

		$(".ct-btn-novo-post-passo-2 .mask-block").show();
		$(".ct-btn-publicar-post .mask-block").show();

		$("#form-inclui-gestor input").val("");

		$(".select-gestor-operacao .simple-select li").removeClass("selected sel-tags-oper-active");
		$(".select-gestor-operacao .trigger-simple-select").removeClass("select-opened trigger-simple-select-active").html("Selecione...").append('<i class="fa fa-caret-up" aria-hidden="true"></i>');

		$(".ct-mask-btn-inclui-gestor .mask-block").show();
	}

	$(".mask-modal-inclui-gestor").on('click',function(e){
			if(e.target == this) {
				limpaModalIncluiGestor();
			}
	});

	$(document).on("keyup", ".input-nome-novo-gestor", function(){
		if($(this).val() != "") {
			$(this).removeClass("borda-vermelha");
			$(".valida-input-nome-novo-gestor").val('1');

			if($(".tipo-nova-operacao").val() == ""){
				$(".select-gestor-operacao .trigger-simple-select").addClass("borda-vermelha");
				$(".valida-select-gestor-operacao").val('0');
			} else {
				$(".select-gestor-operacao .trigger-simple-select").removeClass("borda-vermelha");
				$(".valida-select-gestor-operacao").val('1');
			}

			if($(".valida-input-nome-novo-gestor").val() == "1" && $(".valida-select-gestor-operacao").val() == "1") {
				$(".ct-mask-btn-inclui-gestor .mask-block").hide();
			}

		} else {
			$(this).addClass("borda-vermelha");
			$(".valida-input-titulo-post").val('0');
			$(".ct-mask-btn-inclui-gestor .mask-block").show();
		}
	});



	//Editar gestor
	//Adiciona evento click para os proximos nomes dinamicos.
	$("body").on("click", ".nome-gestor", function() {
		$(".nome-gestor").bind("click");
	});

	$(".nome-gestor").click(function(){
		var id_gestor = $(this).attr("rel");
		$(".hidden-id-gestor").val(id_gestor);
		$(".mask-modal-inclui-gestor").fadeIn();
		$("body").addClass("overflow");

		$.ajax({
				type: 'POST',
				dataType: 'json',
				url: 'functions.php',
				async: true,
				data: {id_gestor: id_gestor, acao: "editaGestor"},
				success: function(data) {
					$(".clo").append(data);
				},
				error: function(data) {
					console.log('oh');
				},
				complete: function(data) {
					console.log(data);
				}
		});
	});



// *USUARIOS

	//Incluir Usuário
	$(document).on("click", "#btn-inclui-usuario", function(){
		var tit = $(".input-nome-novo-usuario").val();
		$("#form-inclui-usuario").append("<input type='hidden' name='acao' value='incluirUsuario' />");
		login = tit.toLowerCase().replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '').replace(/[áàãâä\{\}\[\]\\\/]/gi, "a").replace(/[éêèë\{\}\[\]\\\/]/gi, "e").replace(/[íìïî\{\}\[\]\\\/]/gi, "i").replace(/[óòôõö\{\}\[\]\\\/]/gi, "o").replace(/[úùûü\{\}\[\]\\\/]/gi, "u").split(" ").join("-").trim();
		loginSplit = login.split('-');
		loginUsuario = loginSplit[0]+'.'+loginSplit[1];

		$("#form-inclui-usuario").append("<input type='hidden' class='login-novo-usuario' name='login-novo-usuario' value='"+loginUsuario+"' />");
		var novoUsuario = $("#form-inclui-usuario").serialize();
		console.log(novoUsuario);
		$.ajax({
				type: 'POST',
				url: 'functions.php',
				dataType: "json",
				async: true,
				data: novoUsuario,
				beforeSend: function(data) {
					$("#form-inclui-usuario").hide();
					$("#result-inclui-usuario").show();
					$(".mensagem-result-inclui-usuario-enviando").show();
				},
				complete: function(data) {
					setTimeout(function() {
						$("#form-inclui-usuario").hide();
						$("#result-inclui-usuario").show();
						$(".mensagem-result-inclui-usuario-enviando").hide();
						$(".mensagem-result-inclui-usuario-sucesso").show();
					},2000);
				}
		});
	});

	//add limpa modal ao botao fechar da mesma modal para novo post
	$("body").on("click", ".mask-modal-inclui-usuario .modal-title .btn-close-modal", function(){
		limpaModalIncluiUsuario();
	});

	//Limpa modal novo post
	function limpaModalIncluiUsuario() {
		$(".mask-modal-inclui-usuario").fadeOut();
		$(".mask-modal-inclui-usuario .aba-formulario, .mensagem-result-inclui-usuario").hide();
		$("#form-inclui-usuario").show();
		$(".ct-btn-novo-post-passo-2 .mask-block").show();
		$(".ct-btn-publicar-post .mask-block").show();
		$("#form-inclui-usuario input").val("");
		$(".ct-mask-btn-inclui-usuario .mask-block").show();
	}

	$(".mask-modal-inclui-usuario").on('click',function(e){
			if(e.target == this) {
				limpaModalIncluiUsuario();
			}
	});

	$(document).on("keyup", ".input-nome-novo-usuario", function(){
		if($(this).val() != "") {
			$(this).removeClass("borda-vermelha");
			$(".valida-input-nome-novo-usuario").val('1');

			if($(".input-email-novo-usuario").val() == ""){
				$(".input-email-novo-usuario").addClass("borda-vermelha");
				$(".valida-input-email-novo-usuario").val('0');
			} else {
				$(".input-email-novo-usuario").removeClass("borda-vermelha");
				$(".valida-input-email-novo-usuario").val('1');
			}

			if($(".valida-input-nome-novo-usuario").val() == "1" && $(".valida-input-email-novo-usuario").val() == "1") {
				$(".ct-mask-btn-inclui-usuario .mask-block").hide();
			}

		} else {
			$(this).addClass("borda-vermelha");
			$(".valida-input-nome-novo-usuario").val('0');
			$(".ct-mask-btn-inclui-usuario .mask-block").show();
		}
	});

	$(document).on("keyup", ".input-email-novo-usuario", function(){
		if($(this).val() != "") {
			$(this).removeClass("borda-vermelha");
			$(".valida-input-email-novo-usuario").val('1');

			if($(".input-nome-novo-usuario").val() == ""){
				$(".input-nome-novo-usuario").addClass("borda-vermelha");
				$(".valida-input-nome-novo-usuario").val('0');
			} else {
				$(".input-nome-novo-usuario").removeClass("borda-vermelha");
				$(".valida-input-nome-novo-usuario").val('1');
			}

			if($(".valida-input-nome-novo-usuario").val() == "1" && $(".valida-input-email-novo-usuario").val() == "1") {
				$(".ct-mask-btn-inclui-usuario .mask-block").hide();
			}

		} else {
			$(this).addClass("borda-vermelha");
			$(".valida-input-email-novo-usuario").val('0');
			$(".ct-mask-btn-inclui-usuario .mask-block").show();
		}
	});


//Botao para excluir na propria tabela de resultados
$(document).on("click", ".btn-exclui-inline", function(){
	var excluir = $(this).attr("rel"),
			item_excluir,
			send_ok = false,
	 		id_excluir = $(this).attr("id");

	$(".mask-modal-excluir").fadeIn();
	$("body").addClass("overflow");

	$(document).on("click", "#excluirDefinitivo", function(){
		$.ajax({
				type: 'POST',
				dataType: 'json',
				url: 'functions.php',
				async: true,
				data: {acao: 'excluir', id_excluir: id_excluir, item_excluir: item_excluir},
				complete: function(dados) {}
		});
	});

	switch(excluir) {
		case "btn-exclui-post": {
			item_excluir = 'post';
		};
		break;

		case "btn-exclui-gestor": {
			item_excluir = 'gestor';
		};
		break;

		case "btn-exclui-operacao": {
			item_excluir = 'operacao';
		};
		break;

		case "btn-exclui-usuario": {
			item_excluir = 'usuario';
		};
		break;
	}
});

	//Chars count
	$(".textarea-texto-post").on("keyup", function(){
	  var chars = $(this).val().length;
	  $(".chars-count-red").text(210 - chars);

	  if(chars >= 205){
	    $(".count-chars-textarea").addClass("txt-red");
	  } else {
	    $(".count-chars-textarea").removeClass("txt-red");
	  }
	});


	//Atualiza e corrige retirando a ultima barra proveniente do registro que vem do banco de dados para o nomes das operacoes
	//quando a publicacao é exibida em mais operacoes. No banco, deverá ser gravado com virgula como no ex: 2,4, (para as respectivas operacoes)
	//Sao chamadas novamente em cada select que contenha essa opcao para exibir todas os resultados selecionando todas as operações.
	function tdNomeOperacao() {
		$(".td-nome-operacao").each(function(){
			var txt = $(this).text();
			var pos = txt.lastIndexOf('/');
			txt = txt.substring(0,pos)+txt.substring(pos+1);
			$(this).text(txt);
		});
	}

	tdNomeOperacao();


//trigger muda status do usuario na table
$(document).on("click", ".btn-trigger-altera-status-usuario", function(){
	if($(this).hasClass('altera-status-usuario-open')) {
		$(this).parent().find(".container-lista-altera-status-usuario").hide();
		$(this).removeClass("altera-status-usuario-open");
	} else {
		$(this).parent().find(".container-lista-altera-status-usuario").show();
		$(this).addClass("altera-status-usuario-open");
	}
});

//Lista de status de usuario
$(document).on("click", ".container-lista-altera-status-usuario li", function() {
	var acao = 'alteraStatusUsuario',
			thisEl = $(this),
			thisElHtml = $(this).html(),
			thisId = thisEl.attr("id"),
			thisVal = thisEl.attr("rel"),
			thisUser = thisEl.parent().parent().find(".input-altera-status-usuario").attr("id"),
			thisTrig = thisEl.parent().parent().find(".btn-trigger-altera-status-usuario"),
			altera_status_usuario = thisId,
			id_altera_status_usuario = thisUser;

	thisEl.parent().parent().find(".input-altera-status-usuario").val(thisId);
	thisEl.parent().hide();
	thisTrig.removeClass("altera-status-usuario-open").html("<span class='bullet-status-post'></span> "+thisVal).removeClass('ativo inativo').addClass(thisVal);

	thisTrig.hide();
	thisEl.parent().parent().find(".loader-input").show();

	$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'functions.php',
			async: true,
			data: {acao: acao, altera_status_usuario: altera_status_usuario, id_altera_status_usuario: id_altera_status_usuario},
			complete: function(data) {
				setTimeout(function(){
					thisEl.parent().parent().find(".loader-input").hide();
					thisEl.parent().parent().find(".loader-ok").fadeIn();
					setTimeout(function(){
						thisEl.parent().parent().find(".loader-ok").fadeOut();
						setTimeout(function(){
							thisTrig.show();
						}, 1000);
					}, 1000);
				}, 1000);
			}
	});
});


//trigger muda tipo usuario na table
$(document).on("click", ".btn-trigger-altera-tipo-usuario", function(){
	if($(this).hasClass('altera-tipo-usuario-open')) {
		$(this).parent().find(".container-lista-altera-tipo-usuario").hide();
		$(this).removeClass("altera-tipo-usuario-open");
	} else {
		$(this).parent().find(".container-lista-altera-tipo-usuario").show();
		$(this).addClass("altera-tipo-usuario-open");
	}
});

//Lista de tipos de usuario
$(document).on("click", ".container-lista-altera-tipo-usuario li", function() {
	var acao = 'alteraTipoUsuario',
			thisEl = $(this),
			thisElHtml = $(this).html(),
			thisId = thisEl.attr("id"),
			thisVal = thisEl.attr("rel"),
			thisUser = thisEl.parent().parent().find(".input-altera-tipo-usuario").attr("id"),
			thisTrig = thisEl.parent().parent().find(".btn-trigger-altera-tipo-usuario"),
			altera_tipo_usuario = thisId,
			id_altera_tipo_usuario = thisUser;

	thisEl.parent().parent().find(".input-altera-tipo-usuario").val(thisId);
	thisEl.parent().hide();
	thisTrig.removeClass("altera-tipo-usuario-open").html(thisVal);

	thisTrig.hide();
	thisEl.parent().parent().find(".loader-input").show();

	$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'functions.php',
			async: true,
			data: {acao: acao, altera_tipo_usuario: altera_tipo_usuario, id_altera_tipo_usuario: id_altera_tipo_usuario},
			complete: function(data) {
				setTimeout(function(){
					thisEl.parent().parent().find(".loader-input").hide();
					thisEl.parent().parent().find(".loader-ok").fadeIn();
					setTimeout(function(){
						thisEl.parent().parent().find(".loader-ok").fadeOut();
						setTimeout(function(){
							thisTrig.show();
						}, 1000);
					}, 1000);
				}, 1000);
			}
	});
});


$(document).on("keyup", ".input-table-ajax", function(e){
	if(e.which == 13) {
		var acao = 'alteraTipoUsuario',
				thisEl = $(this),
				thisElHtml = $(this).html(),
				thisId = thisEl.attr("id"),
				thisVal = thisEl.val(),
				thisRel = thisEl.attr("rel");

		switch(thisRel) {
			//Usuarios
			case "altera-nome-usuario": {
				alteraNomeUsuario(thisVal, thisRel, thisId);
			};
			break;

			case "altera-email-usuario": {
				alteraEmailUsuario(thisVal, thisRel, thisId);
			};
			break;

			case "altera-login-usuario": {
				alteraLoginUsuario(thisVal, thisRel, thisId);
			};
			break;

			//Gestores
			case "altera-nome-gestor": {
				alteraNomeGestor(thisVal, thisRel, thisId);
			};
			break;

			case "altera-email-gestor": {
				alteraEmailGestor(thisVal, thisRel, thisId);
			};
			break;

			case "altera-operacao-gestor": {
				alteraOperacaoUsuario(thisVal, thisRel, thisId);
			};
			break;

			//Operacoes
			case "altera-nome-operacao": {
				alteraNomeOperacao(thisVal, thisRel, thisId);
			};
			break;
		}

		function alteraNomeUsuario(thisVal, thisRel, thisId) {
			var acao = 'alteraNomeUsuario';
			$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'functions.php',
					async: true,
					data: {acao: acao, altera_nome_usuario: thisVal, id_altera_nome_usuario: thisId},
					beforeSend: function(data) {
						thisEl.parent().parent().find("#tip-altera-campos").hide();
						thisEl.hide();
						thisEl.parent().parent().find(".loader-input").show();
					},
					success: function(data) {

					},
					error: function(data) {

					},
					complete: function(data) {
						setTimeout(function(){
							thisEl.parent().parent().find(".loader-input").hide();
							thisEl.parent().parent().find(".loader-ok").fadeIn();

							setTimeout(function(){
								thisEl.parent().parent().find(".loader-ok").fadeOut();
								setTimeout(function(){
									thisEl.parent().parent().find("#tip-altera-campos").show();
									thisEl.show();
								}, 1000);
							}, 1000);
						}, 1000);
					}
			});
		}

		function alteraEmailUsuario(thisVal, thisRel, thisId) {
			var acao = 'alteraEmailUsuario';
			$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'functions.php',
					async: true,
					data: {acao: acao, altera_email_usuario: thisVal, id_altera_email_usuario: thisId},
					beforeSend: function(data) {
						thisEl.parent().parent().find("#tip-altera-campos").hide();
						thisEl.hide();
						thisEl.parent().parent().find(".loader-input").show();
					},
					complete: function(data) {
						setTimeout(function(){
							thisEl.parent().parent().find(".loader-input").hide();
							thisEl.parent().parent().find(".loader-ok").fadeIn();

							setTimeout(function(){
								thisEl.parent().parent().find(".loader-ok").fadeOut();
								setTimeout(function(){
									thisEl.parent().parent().find("#tip-altera-campos").show();
									thisEl.show();
								}, 1000);
							}, 1000);
						}, 1000);
					}
			});
		}

		function alteraLoginUsuario(thisVal, thisRel, thisId) {
			var acao = 'alteraLoginUsuario';
			$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'functions.php',
					async: true,
					data: {acao: acao, altera_login_usuario: thisVal, id_altera_login_usuario: thisId},
					beforeSend: function(data) {
						thisEl.parent().parent().find("#tip-altera-campos").hide();
						thisEl.hide();
						thisEl.parent().parent().find(".loader-input").show();
					},
					complete: function(data) {
						setTimeout(function(){
							thisEl.parent().parent().find(".loader-input").hide();
							thisEl.parent().parent().find(".loader-ok").fadeIn();

							setTimeout(function(){
								thisEl.parent().parent().find(".loader-ok").fadeOut();
								setTimeout(function(){
									thisEl.parent().parent().find("#tip-altera-campos").show();
									thisEl.show();
								}, 1000);
							}, 1000);
						}, 1000);
					}
			});
		}

		function alteraNomeGestor(thisVal, thisRel, thisId) {
			var acao = 'alteraNomeGestor';
			$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'functions.php',
					async: true,
					data: {acao: acao, altera_nome_gestor: thisVal, id_altera_nome_gestor: thisId},
					beforeSend: function(data) {
						thisEl.parent().parent().find("#tip-altera-campos").hide();
						thisEl.hide();
						thisEl.parent().parent().find(".loader-input").show();
					},
					complete: function(data) {
						setTimeout(function(){
							thisEl.parent().parent().find(".loader-input").hide();
							thisEl.parent().parent().find(".loader-ok").fadeIn();

							setTimeout(function(){
								thisEl.parent().parent().find(".loader-ok").fadeOut();
								setTimeout(function(){
									thisEl.parent().parent().find("#tip-altera-campos").show();
									thisEl.show();
								}, 1000);
							}, 1000);
						}, 1000);
					}
			});
		}

		function alteraEmailGestor(thisVal, thisRel, thisId) {
			var acao = 'alteraEmailGestor';
			$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'functions.php',
					async: true,
					data: {acao: acao, altera_email_gestor: thisVal, id_altera_email_gestor: thisId},
					beforeSend: function(data) {
						thisEl.parent().parent().find("#tip-altera-campos").hide();
						thisEl.hide();
						thisEl.parent().parent().find(".loader-input").show();
					},
					complete: function(data) {
						setTimeout(function(){
							thisEl.parent().parent().find(".loader-input").hide();
							thisEl.parent().parent().find(".loader-ok").fadeIn();

							setTimeout(function(){
								thisEl.parent().parent().find(".loader-ok").fadeOut();
								setTimeout(function(){
									thisEl.parent().parent().find("#tip-altera-campos").show();
									thisEl.show();
								}, 1000);
							}, 1000);
						}, 1000);
					}
			});
		}

		function alteraNomeOperacao(thisVal, thisRel, thisId) {
			var acao = 'alteraNomeOperacao';
			$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'functions.php',
					async: true,
					data: {acao: acao, altera_nome_operacao: thisVal, id_altera_nome_operacao: thisId},
					beforeSend: function(data) {
						thisEl.parent().parent().find("#tip-altera-campos").hide();
						thisEl.hide();
						thisEl.parent().parent().find(".loader-input").show();
					},
					complete: function(data) {
						setTimeout(function(){
							thisEl.parent().parent().find(".loader-input").hide();
							thisEl.parent().parent().find(".loader-ok").fadeIn();

							setTimeout(function(){
								thisEl.parent().parent().find(".loader-ok").fadeOut();
								setTimeout(function(){
									thisEl.parent().parent().find("#tip-altera-campos").show();
									thisEl.show();
								}, 1000);
							}, 1000);
						}, 1000);
					}
			});
		}
		//add aqui as funcoes
		//add aqui as funcoes - fim
	}

});

});


$(window).load(function() {
	$(".carregando").delay(1000).fadeOut(1000);
  $(".site").fadeIn(1000);
});
